<!--footer infor colomn-->
<div class="footercolmn">
<div class="container">
    <ul>
        <li>
            <i class="frupeeicn sprite"></i>
            <h4>Value for money</h4>
            <p>At CLINITO buy genuine, durable products at competitive rates and get your money’s worth.</p>
        </li>
        <li>
            <i class="fdelvicn sprite"></i>
            <h4>Delivery across India</h4>
            <p>Get your order delivered anywhere in the country at single as well as multiple locations.</p>
        </li>
        <li>
            <i class="fsafeicn sprite"></i>
            <h4>Safe payment options</h4>
            <p>Make safe payments for your orders through secure channels.</p>
        </li>
        <li>
            <i class="fshopicn sprite"></i>
            <h4>Shop with confidence</h4>
            <p>Be completely assured of security and quality while you shop with us.</p>
        </li>
        <li>
            <i class="fhelpicn sprite"></i>
            <h4>24/7 customer assistance</h4>
            <p>Feel free to contact our customer care centers 24/7 for any form of assistance that you may require. </p>
        </li>
    </ul>
</div>
</div>

 