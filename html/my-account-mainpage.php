<?php
include 'header.php';
?>
<!--bredcrumbs-->
<ol class="breadcrumb container">
  	<li><a href="#">Home</a></li>
  	<li><a href="#">My Account </a></li>
  	<li class="active">My Order</li>
</ol>
<div class="container accountDetailssec">
  	<div class="p20"> 
    	<!--My Account details-->
        <div class="row head-acc">
        	<div class="col-md-6 col-sm-6">
            	<h1>MY ACCOUNT</h1>
            </div>
            <div class="col-md-6 col-sm-6 text-right">
            	<div class="dropdown">
  					<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                    	<span class="glyphicon glyphicon-filter"></span> All orders
  						<span class="caret"></span>
                  	</button>
  					<ul class="dropdown-menu">
                        <li><a href="#">All Orders</a></li>
                        <li><a href="#">In Process</a></li>
                        <li><a href="#">Shipped</a></li>
                        <li><a href="#">Partially Shipped</a></li>
                        <li><a href="#">Delivered</a></li>
                        <li><a href="#">Awaiting Payment</a></li>
                        <li><a href="#">Cancelled</a></li>
                        <li><a href="#">Returned</a></li>
                        <li><a href="#">Rejected</a></li>
                        <li><a href="#">Completed</a></li>
  					</ul>
				</div>
            </div>
        </div>
    	<div class="row">
      		<div class="col-md-3 col-sm-3 order-part"> <!-- required for floating -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-left">
                    <li class="active"><a href="#profile" data-toggle="tab"><i class="acc_sprite profile_icn"></i> <p>My Profile</p></a></li>
                    <li><a href="#orders" data-toggle="tab"><i class="acc_sprite order_icn"></i> <p>My Orders</p></a></li>
                    <li><a href="#list" data-toggle="tab"><i class="acc_sprite shipping_icn"></i> <p>Shopping List</p></a></li>
                    <li><a href="#points" data-toggle="tab"><i class="acc_sprite points_icn"></i> <p>My Points</p></a></li>
                    <li><a href="#address" data-toggle="tab"><i class="acc_sprite address_icn"></i> <p>Address Book</p></a></li>
                    <li><a href="#rating" data-toggle="tab"><i class="acc_sprite rate_icn"></i> <p>Reviews & Ratings</p></a></li>
                    <li><a href="#message" data-toggle="tab"><i class="acc_sprite msg_icn"></i> <p>Messages</p></a></li>
                    <li><a href="#password" data-toggle="tab"><i class="acc_sprite pwd_icn"></i> <p>Manage Password</p></a></li>
                    <li><a href="#support" data-toggle="tab"><i class="acc_sprite support_icn"></i> <p>Supports</p></a></li>
                </ul>
            </div>
            <div class="col-md-9 col-sm-9 my_acc">
                <!-- Tab panes -->
                <div class="tab-content tabs-right" id="buyerSec">
                    <div class="tab-pane active" id="profile">
                        <?php include 'buyer/acc_my_profile.php'; ?>
                    </div>
                    <div class="tab-pane" id="orders">
                        <?php include 'buyer/acc_my_orders.php'; ?>
                    </div>
                    <div class="tab-pane" id="list">
                        <?php include 'buyer/acc_shopping_list.php'; ?>
                    </div>
                    <div class="tab-pane" id="points">
                    	<?php include 'buyer/acc_my_points.php'; ?>
                    </div>
                    <div class="tab-pane" id="address">
                    	<?php include 'buyer/acc_address.php'; ?>
                    </div>
                    <div class="tab-pane" id="rating">
                    	<?php include 'buyer/acc_my_rating.php'; ?>
                    </div>
                    <div class="tab-pane" id="message">
                    	<?php include 'buyer/acc_messages.php'; ?>
                    </div>
                    <div class="tab-pane" id="password">
                        <?php include 'buyer/acc_manage_password.php'; ?>
                    </div>
                    <div class="tab-pane" id="support">
                        <?php include 'buyer/acc_support.php'; ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
    	</div>
  	</div>  
</div>
<!--sell on medibridge-->
<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>
