<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  	<li><a href="#">Home</a></li>
  	<li class="active">Best Selling</li>
</ol>
<div class="container bestselling">
	<div class="row">
    	<div class="col-md-12 text-center darkbluebtn box-heading">
        	<h3 class="whiteText">Best Selling Products</h3>
        </div>
    </div>
	<div class="row BrandSlider">
    	<div class="owl-carousel">
      		<div class="item">
      			<div class="rec-industry-list">
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
               	</div>
      		</div>
      		<div class="item">
      			<div class="rec-industry-list">
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
               	</div>
      		</div>
            <div class="item">
      			<div class="rec-industry-list">
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
               	</div>
      		</div>
            <div class="item">
      			<div class="rec-industry-list">
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
               	</div>
      		</div>
            <div class="item">
      			<div class="rec-industry-list">
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
               	</div>
      		</div>
            <div class="item">
      			<div class="rec-industry-list">
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                    <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
                      <div class="product-name"><a href="product_details.php">Experimentation and
                        Research</a></div>
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
               	</div>
      		</div>
    	</div>
  	</div>
</div>
<div class="container selling"> 
	<div class="row">
    	<div class="col-md-12 text-center darkbluebtn box-heading">
        	<h3 class="whiteText">Best Selling Categories</h3>
        </div>
    </div>
  <!--Top sub categories 1 and banner-->
  <div class="row catseclist">
    <div class="col-sm-3 col-md-3 p0-col catlist">
      <h2><span class="diagnosticicn sprite"></span> <span>EXAM AND <br>
        DIAGNOSTIC</span></h2>
      <ul>
        <li><a href="filter.php">AED Defibrillators</a></li>
        <li><a href="Shopping-List-main.php">Blood Pressure & Monitoring</a></li>
        <li><a href="">Diagnostic </a></li>
        <li><a href="">Testing/Monitoring</a></li>
        <li><a href="">Dopplers</a></li>
        <li><a href="">Needles & Syringes</a></li>
        <li><a href="">ECG</a></li>
        <li><a href="">Endoscopic</a></li>
        <li><a href="">Needles & Syringes</a></li>
      </ul>
    </div>
    
    <div class="col-sm-9 col-md-9 p0-col ">
      <div class="rec-industry-list">
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
      </div>
    </div>
  </div>
  <!--Top sub categories 2 and banner-->
  <div class="row catseclist">
    <div class="col-sm-3 col-md-3 p0-col catlist">
      <h2><span class="diagnosticicn sprite"></span> <span>EXAM AND <br>
        DIAGNOSTIC</span></h2>
      <ul>
        <li><a href="filter.php">AED Defibrillators</a></li>
        <li><a href="Shopping-List-main.php">Blood Pressure & Monitoring</a></li>
        <li><a href="">Diagnostic </a></li>
        <li><a href="">Testing/Monitoring</a></li>
        <li><a href="">Dopplers</a></li>
        <li><a href="">Needles & Syringes</a></li>
        <li><a href="">ECG</a></li>
        <li><a href="">Endoscopic</a></li>
        <li><a href="">Needles & Syringes</a></li>
      </ul>
    </div>
    
    <div class="col-sm-9 col-md-9 p0-col ">
      <div class="rec-industry-list">
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
      </div>
    </div>
  </div>
  <!--Top sub categories 3 and banner-->
  <div class="row catseclist">
    <div class="col-sm-3 col-md-3 p0-col catlist">
      <h2><span class="diagnosticicn sprite"></span> <span>EXAM AND <br>
        DIAGNOSTIC</span></h2>
      <ul>
        <li><a href="filter.php">AED Defibrillators</a></li>
        <li><a href="Shopping-List-main.php">Blood Pressure & Monitoring</a></li>
        <li><a href="">Diagnostic </a></li>
        <li><a href="">Testing/Monitoring</a></li>
        <li><a href="">Dopplers</a></li>
        <li><a href="">Needles & Syringes</a></li>
        <li><a href="">ECG</a></li>
        <li><a href="">Endoscopic</a></li>
        <li><a href="">Needles & Syringes</a></li>
      </ul>
    </div>
    
    <div class="col-sm-9 col-md-9 p0-col ">
      <div class="rec-industry-list">
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--sell on medibridge-->

<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>