<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
  <li><a href="#">Lab Supplies </a></li>
  <li><a href="#">Beakers </a></li>
  <li class="active">Heavy Low Scale Beakers by Kimble Chase</li>
</ol>
<div class="container">
  <div class="innerpage">
    <div class=" productCompare">
      <div class="p0-col">
        <table class="table table-striped productcomparetbl">
          <tbody>
            <tr class="imgcompare">
              <td scope="row" ><div class="selectedcomparetext">    You've selected these items to compare.
<a href=""> <span class="backarroworange sprite"></span>Back</a></div></td>
              <td scope="row"  ><div class="comparebox">
             
                <span class="glyphicon glyphicon-remove"> </span>
           	      <a href=""> <img src="images/product4.jpg"  class="img-responsive" > 
                	<h4>Low Form Griffin Beakers by Kimble Chase</h4>
					<p class="font12 graytext">Manuf / Supplier : Kimble Chase</p>
				</a>
                <span class="glyphicon glyphicon-circle-arrow-down "></span>
                </div></td>
              <td scope="row"  ><div class="comparebox">
             
                <span class="glyphicon glyphicon-remove"> </span>
           	      <a href=""> <img src="images/product3.jpg"  class="img-responsive" > 
                	<h4>Low Form Griffin Beakers by Kimble Chase</h4>
					<p class="font12 graytext">Manuf / Supplier : Kimble Chase</p>
				</a>
                <span class="glyphicon glyphicon-circle-arrow-down "></span>
                </div></td>
              <td scope="row"  ><div class="comparebox">
             
                <span class="glyphicon glyphicon-remove"> </span>
           	      <a href=""> <img src="images/product4.jpg"  class="img-responsive" > 
                	<h4>Low Form Griffin Beakers by Kimble Chase</h4>
					<p class="font12 graytext">Manuf / Supplier : Kimble Chase</p>
				</a>
                <span class="glyphicon glyphicon-circle-arrow-down "></span>
                </div></td>
            </tr>
            <tr>
              <th scope="row">Latex Free</th>
               <td scope="row">No</td>
              <td scope="row">No</td>
              <td scope="row">No</td>
            </tr>
            <tr>
              <th scope="row">CAP</th>
              <td scope="row">125Ml</td>
              <td scope="row">125Ml, 200Ml, 250Ml, 600Ml</td>
              <td scope="row">&nbsp;</td>
            </tr>
            <tr>
              <th scope="row">COLOR</th>
               <td scope="row">125Ml</td>
              <td scope="row">Green</td>
              <td scope="row">Red</td>
            </tr>
            <tr>
              <th scope="row">HPIS CLASSIFICATION</th>
              <td scope="row">125Ml</td>
              <td scope="row">220_10_10_20</td>
              <td scope="row">220_10_10_20</td>
            </tr>
            <tr>
              <th scope="row">MATERIAL</th>
               <td scope="row">125Ml</td>
              <td scope="row">Stainless Steel</td>
              <td scope="row">Borosilicate Glass</td>
            </tr>
            <tr>
              <th scope="row">Material Type</th>
              <td scope="row">125Ml</td>
              <td scope="row">&nbsp;</td>
              <td scope="row">Glass</td>
            </tr>
            <tr>
              <th scope="row">PRODUCT TYPE</th>
               <td scope="row">125Ml</td>
              <td scope="row">No</td>
              <td scope="row">Low Form Heavy Duty Beakers</td>
            </tr>
            <tr>
              <th scope="row">STERILE</th>
              <td scope="row">125Ml</td>
              <td scope="row">125 ml, 200 ml, 250 ml, 600 ml</td>
              <td scope="row">&nbsp;</td>
            </tr>
            <tr>
              <th scope="row">Size in ML</th>
               <td scope="row">125Ml</td>
              <td scope="row">42211611</td>
              <td scope="row">&nbsp;</td>
            </tr>
            <tr>
              <th scope="row">UNSPSC</th>
               <td scope="row">125Ml</td>
              <td scope="row">&nbsp;</td>
              <td scope="row">&nbsp;</td>
            </tr>
            <tr>
              <th scope="row">Price</th>
              <td scope="row"><span class="rupee">₹</span>5,000/-</td>
              <td scope="row"><span class="rupee">₹</span>5,000/-</td>
              <td scope="row"><span class="rupee">₹</span>5,000/-</td>
            </tr>
            <tr>
              <th scope="row">&nbsp;</th>
              <td scope="row" class=" "><div class="pricehld">
              <input name="" type="button" class="btn orangebtn dtlBuyNowBtn" value="Buy Now!">
              <button name="" type="button" class="btn borderbtn  dtladdToCartBtn" ><span class="sprite"></span> <span>Add to Cart</span></button>
            </div></td>
              <td scope="row"><div class="pricehld">
              <input name="" type="button" class="btn orangebtn dtlBuyNowBtn" value="Buy Now!">
              <button name="" type="button" class="btn borderbtn  dtladdToCartBtn" ><span class="sprite"></span> <span>Add to Cart</span></button>
            </div></td>
              <td scope="row"><div class="pricehld">
              <input name="" type="button" class="btn orangebtn dtlBuyNowBtn" value="Buy Now!">
              <button name="" type="button" class="btn borderbtn  dtladdToCartBtn" ><span class="sprite"></span> <span>Add to Cart</span></button>
            </div></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!--sell on medibridge-->

<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>
