<?php
	include 'header.php';
?>
<!--bredcrumbs-->
<ol class="breadcrumb container">
  	<li><a href="#">Home</a></li>
  	<li class="active">Cancel Request</li>
</ol>
<div class="container cancelRequest p20">
    <div class="row m0">
    	<div class="col-md-6 col-sm-6 block1 p10">
        	<div class="sub_block">
            	<h3>Item Details</h3>
                <table width="100%" class="tableData">
                	<tr>
                    	<td width="30%">
                        	<a href=""><img src="images/product_300.jpg" width="60%"></a>
                        </td>
                        <td width="70%">
                        	<h5>Product Name</h5>
                            <ul class="productList">
                            	<li>Color : Black</li>
                                <li>Price : <span class="rupee">₹</span> 3000</li>
                                <li>Quantity : 1</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 block1 p10">
        	<div class="sub_block">
            	<h3>Pick Up Details <span class="pull-right" style="margin-top:-8px;"><button class="btn greenbtn">Change</button></span></h3>
                <table width="100%" class="tableData">
                	<tr>
                    	<td width="100%">
                        	<ul>
                            	<li>Name : Nitin Kawale</li>
                                <li>Address : 203, Block C, Sunmill compound, <br />Lower Parel, Mumbai 400 034.</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row m0">
    	<div class="col-md-12 col-sm-12 block1 p10 mt20">
        	<div class="sub_block1">
            	<h3 class="orangetext">Easy Cancel</h3>
                <table width="100%" class="tableData">
                	<tr>
                    	<td width="100%">
                        	<form name="frm_cancel" class="form-horizontal" enctype="multipart/form-data">
                            	<div class="form-group">
                                	<div class="col-md-4">
                                		<label>1. Reason for Cancel</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-md-2 text-right">Reason for cancel :</div>
                                    <div class="col-md-4">
                                    	<select class="form-control">
                                        	<option value="Late Delivery">Late Delivery</option>
                                            <option value="Delivery Not on Time">Delivery Not on Time</option>
                                            <option value="Placed order By mistake">Placed order By mistake</option>
                                            <option value="Wrong Item Ordered">Wrong Item Ordered</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-md-2 text-right">Issue with item :</div>
                                    <div class="col-md-4">
                                    	<select class="form-control">
                                        	<option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-md-2 text-right">Comments :</div>
                                    <div class="col-md-9">
                                    	<textarea class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-md-4">
                                		<label>2. Request Cancel</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-md-2 text-right">I want :</div>
                                    <div class="col-md-4">
                                    	<select class="form-control">
                                        	<option value="Refund To Original">Refund To Original</option>
                                            <option value="Delivery Not on Time">Delivery Not on Time</option>
                                            <option value="Placed order By mistake">Placed order By mistake</option>
                                            <option value="Wrong Item Ordered">Wrong Item Ordered</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-md-2 text-right">&nbsp;</div>
                                    <div class="col-md-9">
                                    	<div class="alert alert-warning">
                                          	Your amount will be refunded to your original payment account.
										</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<div class="col-md-11 col-sm-11 block1 p10 text-right">
                                        <button class="btn darkgraybtn">Cancel</button>
            							<button class="btn orangebtn">Request Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                </table>
          	</div>
      	</div>
    </div>
</div>
<?php
	include 'footer-top.php';
?>
<?php
	include 'footer.php';
?>