<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
 
  <li class="active">Making Payments</li>
</ol>
<div class="container">
  <div class="innercontent makingpaymentwrap ">
    <div class="p20"> 
     <h1 class="titleh">Making Payments</h1>
     <div class="bgwhite ">
     	<h2>Avail the benefits of multiple payment options for any order:</h2>
        <ul class="optin">
        	<li>
            	<h3>Cash on delivery</h3>
                <p>Pay for your order at the time of delivery.</p>
            </li>
            <li>
            	<h3>Online payment</h3>
                <p>Use your financial credentials for online payment of your order.</p>
            </li>
            <li>
            	<h3>Net Banking</h3>
                <p>Use internet banking facilities for payment.</p>
            </li>
            <li>
            	<h3>NEFT</h3>
                <p>Pay up to Rs. 10 Lakh. Receiver gets money in 2 to 24 hours.</p>
            </li>
            <li>
            	<h3>RTGS</h3>
                <p>Pay min Rs. 2 Lakh, max Rs. 10 Lakh. Real time transaction.</p>
            </li>
        </ul>
        
        
     </div>
    </div>
  </div>
</div>
<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>
