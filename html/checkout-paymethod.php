
		<?php include 'header.php'; ?>
		<div class="container" id="checkoutBox">
        	<div class="row">
            	<div class="col-md-12">
                	<div class="row nav-box">
                        <div class="col-md-12">
                        	<ul class="checkoutNav">
                            	<li class="active">
                                	<a href="checkout-shoppingcart.php"><h4><span class="iconbox">1</span> Shopping Cart</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li class="active">
                                	<a href="checkout-yourdetails.php"><h4><span class="iconbox">2</span> Your Details</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li class="active">
                                	<a href="checkout-paymethod.php"><h4><span class="iconbox">3</span> Payment Methods</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li>
                                	<a href="checkout-confirmation.php"><h4><span class="iconbox">4</span> Comfirmation</h4></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row nav-box1">
                    	<div class="col-md-9 col-sm-12 padding-mobile">
                        	<div class="col-md-3 col-sm-3 order-part"> <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active"><a href="#COD" data-toggle="tab">CASH ON DELIVERY</a></li>
                                    <li><a href="#Online" data-toggle="tab">PAY ONLINE</a></li>
                                    <li><a href="#NEFT" data-toggle="tab">NEFT/RTGS</a></li>
                                </ul>
                            </div>
                            <div class="col-md-9 col-sm-9 order-part-payment">
                                <!-- Tab panes -->
                                <div class="tab-content tabs-right">
                                    <div class="tab-pane active" id="COD">
                                    	<div class="box">
                                        	<p>This cash on delievery order requires verification. We will send you SMS with One Time Password(OTP) on this number.</p>
                                            <br />
                                            <form name="frm_cod" method="post">
                                            	<div class="form-group">
                                                	<div class="col-md-6 col-sm-6 p0">
                                                    	<input type="text" class="form-control" placeholder="Enter Mobile No.">
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    	<input type="submit" class="btn lightgraybtn" value="Send OTP">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="box">
                                        	<p>OTP send to your mobile numbar eg. 9876****10 (<a href="" class="bluetext">Change</a>)</p>
                                            <br />
                                            <form name="frm_cod" method="post" class="form-horizontal">
                                            	<div class="form-group">
                                                	<div class="col-md-6 col-sm-6">
                                                    	<input type="text" class="form-control" placeholder="Enter OTP">
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    	<input type="submit" class="btn lightgraybtn" value="Verify Numbar">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-3 col-sm-3">
                                                    	<input type="submit" class="btn lightgraybtn" value="Resend OTP">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Online">
                                    	<h4>Need Text</h4>
                                       	<br />
                                    </div>
                                    <div class="tab-pane" id="NEFT">
                                    	<div class="box">
                                        	<h4>Bank Details</h4>
                                            <h5>Name: Medeness Pvt. Ltd.</h5>
                                            <h5>Bank Name: Kotak Bank</h5>
                                            <h5>Branch: Lower Parel</h5>
                                            <h5>Ac/No.: 97123456789</h5>
                                            <h5>IFSC: KKBK0000777</h5>
                                            <h5>Branch Code: 1520</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php include 'order-summary.php'; ?>
                 	</div>
                </div>
           	</div>
		</div>
		<?php include 'footer.php'; ?>