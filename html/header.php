<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clinito | Home</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/jquery.fancybox.css" rel="stylesheet">
<link href="css/bootstrap.vertical-tabs.css" rel="stylesheet">
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<link href="css/style_seller.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/style_g.css" rel="stylesheet">
<link href="css/style-res.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="body">
    <style type="text/css">
		#loader-wrapper {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			z-index: 1000;
			background-color:rgba(255,255,255,0.9);
			display:table;
		}
		#loader{
			height:100%;
			display:table-cell;
			vertical-align:middle;
			text-align:center;
		}
	</style>
	<div id="loader-wrapper">
        <div id="loader">
        	<img src="images/ajax-loader.gif">
        </div>
    </div>    
<!---->
<!-- Modified navbar: Animating from right to left (off canvas) -->
<nav id="navbar2" class="navbar navbar-default visible-xs" role="navigation">
   
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button class="btn4 btn-clean" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3"> <span class="glyphicon glyphicon-menu-hamburger"></span> </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
    <div class="backbtn">
      <button class="btn4 btn-link" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3"> <span class="glyphicon glyphicon-chevron-left"></span> BACK </button>
      </div>
      <div class="navigation">
      
        <div class="filter-options">
          <h4 class="tname"><i class="glyphicon glyphicon-log-in"></i> LOGIN </h4>
        </div>
        <div class="filter-options">
          <h4 class="tname"><i class="glyphicon glyphicon-user"></i> MY ACCOUNT<span class="sprite"></span></h4>
          <div class="filteroption">
            <div class="categoriesfilter">
                <ul class="vscroll">
                  <li  class="closed" >
                    <a href="javascript:void(0)" class="subdropdown headingcat">Lab Supplies</a>
                    <ul>
                      <li class="">
                      <a href="" class="">Beakers</a>
                         
                      </li>
                      <li class=""><a href="" >Blood Collection</a></li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                  <li class="closed" >
                    <a href="javascript:void(0)" class="subdropdown headingcat "  >Anesthesia</a>
                    <ul>
                      <li class="closed"><a href="javascript:void(0)" class="subdropdown headingcat " >Blood Collection</a>
                    
                        <ul>
                          <li class=""><a href="" >Beakers</a></li>
                          <li class=""><a href="" >Blood Collection</a></li>
                          <li class=""><a href="" >Caseettes</a></li>
                        </ul>
                      </li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                </ul>
              
            
          </div>
            
          </div>
        </div>
        <div class="filter-options">
          <h4 class="tname  ">Shop By Category<span class="sprite"></span></h4>
          <div class="filteroption">
            <div class="categoriesfilter">
                <ul class="vscroll">
                  <li  class="closed" >
                    <a href="javascript:void(0)" class="subdropdown headingcat">Lab Supplies</a>
                    <ul>
                      <li class="">
                      <a href="" class="">Beakers</a>
                         
                      </li>
                      <li class=""><a href="" >Blood Collection</a></li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                  <li class="closed" >
                    <a href="javascript:void(0)" class="subdropdown headingcat "  >Anesthesia</a>
                    <ul>
                      <li class="closed"><a href="javascript:void(0)" class="subdropdown headingcat " >Blood Collection</a>
                    
                        <ul>
                          <li class=""><a href="" >Beakers</a></li>
                          <li class=""><a href="" >Blood Collection</a></li>
                          <li class=""><a href="" >Caseettes</a></li>
                        </ul>
                      </li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                </ul>
              
            
          </div>
            
          </div>
        </div>

		<div class="filter-options">
          <h4 class="tname">Shopping List </h4>
        </div>
        <div class="filter-options">
          <h4 class="tname">LOGIN </h4>
        </div>
      </div>
    </div>
    <!-- /.navbar-collapse --> 
  
  <!-- /.container-fluid --> 
</nav>
<div class="header-container"> 
  <!--Top menu-->
  <div class="container toplinks hidden-xs">
    <div class="row">
      <div class="col-sm-7 col-xs-6 col-md-6 p0">
        <div class="pull-left ">
          <ul class="userMenu">
            <li>Customer Care +91 982341234 </li>
            <li>|</li>
            <li>Free Shipping across all over India</li>
          </ul>
        </div>
      </div>
      <div class="col-sm-5 col-xs-6 col-md-6">
        <div class="pull-right">
          <ul class="userMenu">
            <li><a href="#">Track Order</a></li>
            <li><a href="#">Buyer Protection</a></li>
            <li><a href="#">Help </a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <header id="topNavmain">
    <div class="container">
      <div class="row"> 
        <!-- Logo -->
        <div class="col-sm-3 col-xs-8 p0">
          <div class="logo"> <a href="index.php"><img src="images/Clinito_Final_Logo.svg" class="img-responsive"> </a> </div>
        </div>
        <!-- End Logo --> 
        <!-- Search Form -->
        <div class="col-sm-9 col-xs-4 p0-col">
          <div class="input-prepend topsearchbar hidden-xs">
            <div class="btn-group">
              <button class="btn dropdown-toggle" data-toggle="dropdown"> <span id="dropdownSearchText">All Categories</span> <span class="caret"></span> </button>
              <div class="dropdown-menu" id="searchAllCategories" >
                  <ul class="vscroll_category">
                    <li>All Categories</li>
                   <li>Blood Pressure & Monitoring</li>
                   <li>AED Defibrillators</li>
                   <li>Diagnostic</li>
                   <li>Testing Monitoring</li>
                   <li>Dopplers</li>
                   <li>Needles & Syringes</li>
                   <li>ECG</li>
                   <li>Endoscopic</li>
                   <li>Blood Pressure & Monitoring</li>
                   <li>Needles & Syringes</li>
                   <li>All Categories</li>
                   <li>Blood Pressure & Monitoring</li>
                   <li>AED Defibrillators</li>
                   <li>Diagnostic</li>
                   <li>Testing Monitoring</li>
                   <li>Dopplers</li>
                   <li>Needles & Syringes</li>
                   <li>ECG</li>
                   <li>Endoscopic</li>
                   <li>Blood Pressure & Monitoring</li>
                   <li>Needles & Syringes</li>
                   <li>All Categories</li>
                   <li>Blood Pressure & Monitoring</li>
                   <li>AED Defibrillators</li>
                   <li>Diagnostic</li>
                   <li>Testing Monitoring</li>
                   <li>Dopplers</li>
                   <li>Needles & Syringes</li>
                   <li>ECG</li>
                   <li>Endoscopic</li>
                   <li>Blood Pressure & Monitoring</li>
                   <li>Needles & Syringes</li>
                  </ul>
              </div>
            </div>
           <!-- <input class="" id="prependedDropdownButton" type="text" placeholder="I want to buy...">
            <input  type="submit" class="sprite" value="" />-->
            
<input type="hidden" name="topCatSelected" id="topCatSelected" value="all-categories"/>
<input class="" id="prependedDropdownButton" type="text" placeholder="I want to buy...">
<input  type="submit" class="sprite" id="topSearchBut" value="" />
<div id="prediction">
<ul>
<li><a href="">buyer my account</a></li>
<li><a href="">buyer my account</a></li>
<li><a href="">buyer my account</a></li>
</ul>

<h3>PRODUCT CATEGORIES</h3>
<ul>
<li><a href="">buyer my account</a></li>
<li><a href="">buyer my account</a></li>
<li><a href="">buyer my account</a></li>
</ul>
</div>
<!--<div id="searchload"></div>-->
            
          </div>
          <!-- End Search Form --> 
          <!-- Shopping Cart List -->
          <ul class="top-nav-links  ">
            
            <li class="signinbar  dropdown hidden-xs"> <a href="login.php" class="dropdown-toggle"  ><i class="sprite"></i>
              <h5>Sign In</h5>
              <!--<p>Register</p>--> 
              </a>
              <ul id="login-dp" class="dropdown-menu ">
                <li>
                  <div class="row">
                    <div class="col-md-12"> 
                      <!--Login via
                                            	<div class="social-buttons"> 
                                           		<a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a> 
                                            	<a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a> </div>
                                            	OR-->
                      <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                        <div class="form-group">
                          <label class="sr-only" for="exampleInputEmail2">Email address</label>
                          <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                        </div>
                        <div class="form-group">
                          <label class="sr-only" for="exampleInputPassword2">Password</label>
                          <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                          <!--<div class="help-block text-right"><a href="">Forgot Password?</a></div>-->
                        </div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-block yellobtn">Sign in</button>
                        </div>
                        <!--<div class="checkbox">
											 		<label>
											 			<input type="checkbox"> keep me logged-in
											 		</label>
												</div>-->
                      </form>
                    </div>
                    <div class="bottom text-center"> New here ? <a href="#"><b>Join Us</b></a> </div>
                  </div>
                </li>
              </ul>
            </li>
            <li class="shoppinglistbar dropdown hidden-xs"> <a href="Shopping-List-main.php" class="dropdown-toggle"  ><i class="sprite"></i>
              <h5>Shopping List</h5>
              <!-- <p>How it works?</p>--> 
              </a>
              <ul class="dropdown-menu listdropdown">
                <li><a href="">How it works</a></li>
                <li><a href="">Default List</a></li>
                <li><a href="">My Nursing Home List </a></li>
              </ul>
            </li>
            <li class="signinbar afterSignin dropdown hidden hidden-xs"> <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="sprite"></i>
              <h5>Hi Karishma!</h5>
              </a>
              <ul class="dropdown-menu listdropdown ">
                <li><a href=""><span class="sprite myaccounticn"></span>My Account</a></li>
                <li><a href=""><span class=" myordericn"><img src="images/svg/myordericnt.svg"></span>My Orders</a></li>
                <li><a href=""><span class=" myquotesicn"><img src="images/svg/myquotesicn.svg"></span>My Quotes</a></li>
                <li><a href=""><span class=" myprofileicn"><img src="images/svg/myprofileicn.svg"></span>My Profile</a></li>
                <li><a href=""><span class="sprite mypointicn"></span>My Points</a></li>
                <li><a href=""><span class="reviewicn"><img src="images/svg/reviewicn.svg"></span>Review and Ratings</a></li>
                <li><a href=""><span class=" addressicn"><img src="images/svg/addressicn.svg"></span>Address Book</a></li>
                <li><a href=""><span class=" mangaeicn"><img src="images/svg/mangaeicn.svg"></span>Manage Password</a></li>
                <li><a href=""><span class=" signout"><img src="images/svg/signout.svg"></span>Sign Out</a></li>
              </ul>
            </li>
            <li class="cartcounter dropdown"> <a href="checkout-shoppingcart.php" class="dropdown-toggle" ><i class="sprite"></i><span class="cartcounttop">100</span>
              <h5 class="hidden-xs">Cart</h5>
              </a>
              <ul class="dropdown-menu hidden-xs ">
                <li>
                  <div class="row">
                    <div class="col-md-12"> Cart is empty (0) 
                      <!--<ul class=" ">
											<li>
										<table class="table table-striped hcart">
											<tbody><tr>
												<td class="text-center">
													<a href="product.html">
														<img src="images/product-images/hcart-thumb1.jpg" alt="image" title="image" class="img-thumbnail img-responsive">
													</a>
												</td>
												<td class="text-left">
													<a href="product-full.html">
														Seeds
													</a>
												</td>
												<td class="text-right">x 1</td>
												<td class="text-right">$120.68</td>
												<td class="text-center">
													<a href="#">
														<i class="fa fa-times"></i>
													</a>
												</td>
											</tr>
											<tr>
												<td class="text-center">
													<a href="product.html">
														<img src="images/product-images/hcart-thumb2.jpg" alt="image" title="image" class="img-thumbnail img-responsive">
													</a>
												</td>
												<td class="text-left">
													<a href="product-full.html">
														Organic
													</a>
												</td>
												<td class="text-right">x 2</td>
												<td class="text-right">$240.00</td>
												<td class="text-center">
													<a href="#">
														<i class="fa fa-times"></i>
													</a>
												</td>
											</tr>
										</tbody></table>
									</li>
									<li>
										<table class="table table-bordered total">
											<tbody>
												<tr>
													<td class="text-right"><strong>Sub-Total</strong></td>
													<td class="text-left">$1,101.00</td>
												</tr>
												<tr>
													<td class="text-right"><strong>Eco Tax (-2.00)</strong></td>
													<td class="text-left">$4.00</td>
												</tr>
												<tr>
													<td class="text-right"><strong>VAT (17.5%)</strong></td>
													<td class="text-left">$192.68</td>
												</tr>
												<tr>
													<td class="text-right"><strong>Total</strong></td>
													<td class="text-left">$1,297.68</td>
												</tr>
											</tbody>
										</table>
										<p class="text-right btn-block1">
											<a href="cart.html">
												View Cart
											</a>
											<a href="#">
												Checkout
											</a>
										</p>
									</li>									
								</ul>--> 
                      
                    </div>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- End Shopping Cart List --> 
      </div>
    </div>
  </header>
</div>
