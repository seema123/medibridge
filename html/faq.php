<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
  <li class="active">Faq</li>
</ol>
<div class="container">
  <div class=" innercontent faqwrap">
    <h1 class="titleh">Frequently Asked Questions </h1>
    <div class="p20 bgwhite">
    <div class="row">
    	<div class="col-sm-8">  <form class="form ">
        <div class="input-group text-center mb5">
          <input type="text" class="form-control input-lg" placeholder="What can we help with?">
          <span class="input-group-btn">
          <button class="btn btn-lg bluebtn" type="button">Search</button>
          </span> </div>
      </form>
      <p class="help-text mb20 graytext font12" >ex : Returns, Exchange, Damaged product</p>
     
     <div class="faqhld">
        <div class="filter-options">
          <h4 class="tname"><i class="faqordericn"></i>Order<span class="sprite"></span></h4>
          <div class="filteroption">
            <div class="categoriesfilter">
              <ul>
                <li  class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How do I track my order?</a>
                  <ul>
                    <li class=""> An SMS with waybill no. will be sent to your registered phone number. Please check the courier company's website with the waybill number to track your order. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >I missed the delivery of my order today how do I get it delivered again?</a>
                  <ul>
                    <li class=""> We will call you to confirm your availability and reschedule the delivery for you. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How long would it take to get my order delivered?</a>
                  <ul>
                    <li class=""> Most orders take around 7 business days to get processed. A few items and some locations require slightly longer due to logistics issues. Check your order details in My Orders. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Can package be accepted after checking the contents inside it?</a>
                  <ul>
                    <li class=""> A package can’t be opened before delivery is accepted as per our company policy. But you may accept the package & get in touch with us later in case you have any concerns. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What should I do in case my order is delayed?</a>
                  <ul>
                    <li class=""> It rarely happens that an order gets delayed, nevertheless you can check your email and messages for an updated delivery timeline OR you can call the support number and speak to one of our account managers. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What should I do if my order is approved but not yet dispatched for a long time?</a>
                  <ul>
                    <li class=""> Please help us look into it by contacting our Customer Support. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Will the delivery be re-attempted if I’m not able to collect my order the first time?</a>
                  <ul>
                    <li class=""> Yes, we’ll make sure that the delivery is re-attempted the next working day if you can’t collect your order the first time. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Can I change the time of delivery for my order?</a>
                  <ul>
                    <li class=""> Sorry, time of delivery for an order cannot be changed. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > The package I received was broken and the product inside damaged at the time of delivery. What should I do?</a>
                  <ul>
                    <li class="">•	You can request for the item to be replaced or returned by using our return feature.</li>
                    <li class="">•	The replacement will be delivered to you in the next 5-7 days or if you choose to return the consignment, you will be offered a full refund. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Will I receive a call before CLINITO attempts to deliver the package at my place?</a>
                  <ul>
                    <li class="">•	The courier person will call you before delivery only if he cannot find your location with the address shared. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >I have a complaint about the courier who came to deliver my order.</a>
                  <ul>
                    <li class="">•	Please let us know the details of the transgression by contacting our Customer Support.</li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >I am not able to track my order. Please help.</a>
                  <ul>
                    <li class="">•	Please check after 24 hours as the tracking ID for your order will take some time to be activated once it’s shipped. </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="filter-options closed">
          <h4 class="tname closed "><i class="faqcanicn"></i>Cancellation and return policy:<span class="sprite"></span></h4>
          <div class="filteroption" style="display:none">
            <div class="categoriesfilter">
              <ul>
                <li  class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How can I cancel my order?</a>
                  <ul>
                    <li class=""> Orders once booked are delivered by default. In case that you feel that you need to cancel the order, please email us at <a href="mailto:info@clinito.com">info@clinito.com</a> or call us on our toll free number. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What is your return policy? </a>
                  <ul>
                    <li class="">
                      <p>CLINITO will replace or refund you only under the following circumstances </p>
                      <p>•	The product that you received was damaged and/or in an unusable condition <br>
                        •	The product was sent to you is not what you had ordered.<br>
                        •	The product was faulty (wrong quantity was delivered). <br>
                        •	There was a delayed delivery.</p>
                    </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > When are returns not possible?</a>
                  <ul>
                    <li class="">
                      <p>Returns are not possible when: </p>
                      <p> • 	When any consumable item has been used or installed<br>
                        •	When things like price tags, labels, original packing, freebies & accessories are missing at the time of return.<br>
                        •	When items are tampered with or have missing serial numbers <br>
                      </p>
                    </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What should I do if I find the package open or tampered on delivery?</a>
                  <ul>
                    <li class=""> Please email us at info@CLINITO.com or call customer support, we will assist you accordingly. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What should I do if there are hardware/component/functional issues with the device I bought from CLINITO?</a>
                  <ul>
                    <li class="">If the device is not functioning as it is supposed to, it can be defined as damaged/faulty product. Please initiate the return of the device and we shall send a replacement or issue a full refund. If you encounter any difficulties or have any queries, please call Customer Support on our toll free number. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >I ordered a wrong item. Can I return it?</a>
                  <ul>
                    <li class=""> Sorry, you cannot return a wrongly ordered item as all purchases are final. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How will I get my refund for returning an item if I paid COD?</a>
                  <ul>
                    <li class=""> We will initiate a bank transfer to return your money. You will have to provide your bank account details and IFSC code. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How do I find out the status of my refund?</a>
                  <ul>
                    <li class="">
                      <p>The following is the timeline for refunds</p>
                      <p> • 	 	For cancellation before shipping, refunds are processed immediately<br>
                        •	 	For other issues refund will be initiated after we receive and check the returned goods. 
                    </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How do I choose the mode of refund when I’m returning a product?</a>
                  <ul>
                    <li class="">
                      <p> •	You can choose the mode of refund for your product after you’ve entered your return details.<br>
                        •	For Cash on Delivery orders: Please email us your bank details to initiate NEFT<br>
                        •	Pre-paid: Refund will be back to source.</p>
                    </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > When are Refunds provided?</a>
                  <ul>
                    <li class="">
                      <p>Refunds are provided when </p>
                      <p> • 	 	You made a prepaid order and we were unable to service your order.<br>
                        •	 	The seller cannot provide a replacement for a wrong/damaged delivery<br>
                        •	 	A dispute has been ruled in your favour as per Buyer Protection<br>
                      </p>
                    </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > When are cancellations acceptable?</a>
                  <ul>
                    <li class="">
                      <p>Cancellations are accepted by CLINITO when: </p>
                      <p> •	If your pin code is not catered to by any of our courier partners<br>
                        •	If the procurement is taking longer than the specified number of working days and you are not willing to wait further<br>
                        •	You can always check My Orders page to see the status of your order. </p>
                    </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What is CLINITO’s Replacement Guarantee? </a>
                  <ul>
                    <li class=""> If you have received a product in a damaged or in defective condition or it is not as described, you can return it to get a brand new replacement at no extra cost. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >If I request for a replacement, when can I expect it? </a>
                  <ul>
                    <li class=""> The order replacement is initiated after the receiving the originally delivered item. Please check the SMS & email we send you for more details or contact customer support. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > If I have ordered several items, can I return one or more items from that consignment or do I have to return the entire order?</a>
                  <ul>
                    <li class=""> You don’t have to return the entire order, you can return any number of items from your order. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Can I return the items after the return policy period expires? </a>
                  <ul>
                    <li class=""> Sorry, returns are not possible after the return policy period. </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="filter-options closed">
          <h4 class="tname closed"><i class="faqpaymenticn"></i>Payment FAQs<span class="sprite"></span></h4>
          <div class="filteroption" style="display:none">
            <div class="categoriesfilter">
              <ul>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What is Cash on Delivery?</a>
                  <ul>
                    <li class=""> •	Cash on Delivery (CoD), enables you to pay for your order at the time it is delivered to you.<br>
                      •	You may also pay with your Credit or Debit card if the courier person is carrying a card machine. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > My payment process was interrupted. What should I do?</a>
                  <ul>
                    <li class=""> Contact customer support to check your payment status, if it was successful the order will be placed.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What are the modes pf payment for an order?</a>
                  <ul>
                    <li class="">
                      <p>You can choose from the following modes of payment:</p>
                      •	Cash on Delivery<br>
                      •	Net Banking<br>
                      •	Visa, MasterCard, Maestro and American Express Credit or Debit cards issued in India and 21 other countries<br>
                      •	NEFT/RTGS </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Can I use Credit/Debit card or Net banking to pay on CLINITO through my mobile?
                  How do I pay using a credit or debit card? </a>
                  <ul>
                    <li class=""> •	Keep your card number, expiry date, three-digit CVV number ready (found on the backside of your card)<br>
                      •	For extra security, you’ll need to use your card’s online password (Verified by Visa, MasterCard SecureCode etc.) <br>
                      •	Enter the details when you are prompted to & your order will be successfully placed. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Can I pay with any Credit card?</a>
                  <ul>
                    <li class=""> You can use any Visa, MasterCard or American Express Credit cards issued in India to make payments on CLINITO. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Can I use any Debit card to pay? </a>
                  <ul>
                    <li class=""> You can use any Visa, MasterCard or Maestro Debit cards to make payments on CLINITO. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What all card information does CLINITO store?</a>
                  <ul>
                    <li class=""> We currently do not store any card information and all transactions happen through authorized and secure payment gateways.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Is it safe to use my credit or debit card on CLINITO? </a>
                  <ul>
                    <li class=""> Absolutely! We use the highest possible security (256-bit encryption) to protect your card details. Payments are processed through secure gateways managed by leading banks</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Can I use Cash on Delivery (CoD) payment option for every product I buy on CLINITO?</a>
                  <ul>
                    <li class=""> COD may not be available in every Pin Code, please enter your Pin Code on product page to check it can be serviced. Apart from that, COD will be made available only on certain products.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How does CLINITO prevent card fraud?</a>
                  <ul>
                    <li class=""> Fraud detection and prevention are of paramount importance to us. We take numerous steps ensure the genuine and authentic transactions and keep our customers’ details completely secure. Online payments are monitored continuously for suspicious activity and some transactions are verified manually if we feel that it’s not authorized by the owner of the card.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > I want to order large quantities of the product as part of a corporate order, how can I do that?</a>
                  <ul>
                    <li class=""> You can write to <a href="mailto:sales@CLINITO.com">sales@CLINITO.com</a> or call customer support during our stated working hours, we would be glad to help you out. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How do I get the 3D Secure password for my Credit/Debit card?</a>
                  <ul>
                    <li class=""> You can get a 3D Secure password by registering your Credit/Debit card from your bank's website</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How does the 3D Secure password help me in my online transactions? </a>
                  <ul>
                    <li class=""> The 3D Secure password is known only to you ensuring safety when you shop online. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What is a 3D Secure password?</a>
                  <ul>
                    <li class="" > A 3D Secure password is a security measure from VISA & MasterCard that adds an extra layer of security through identity verification for your online Credit & Debit card transactions.</li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="filter-options closed">
          <h4 class="tname closed"><i class="faqshoppingicn"></i>SHOPPING<span class="sprite"></span></h4>
          <div class="filteroption"  style="display:none">
            <div class="categoriesfilter">
              <ul>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Is it necessary to have an account to shop on CLINITO?</a>
                  <ul>
                    <li class="">  	No, It's not. You can shop as a guest but you will have to provide us with a valid e-mail, phone number and address for us to send you the goods </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Why do I see a ‘delivery charge’?</a>
                  <ul>
                    <li class=""> 	If your order value is less than Rs. 1000, a delivery charge is added to your order amount.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Is installation offered for products?</a>
                  <ul>
                    <li class="">
                      	CLINITO doesn’t offer installation services on any products yet, but we will soon.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Do you deliver internationally?</a>
                  <ul>
                    <li class="">  No we only deliver within India. </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="filter-options closed">
          <h4 class="tname closed"><i class="faqwarrentyicn"></i>WARRANTY<span class="sprite"></span></h4>
          <div class="filteroption"  style="display:none">
            <div class="categoriesfilter">
              <ul>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Is any warranty applicable on the products I buy on CLINITO?</a>
                  <ul>
                    <li class="">   All products come with manufacturer warranty. You can check with the manuals provided with the product or the manufacturer's website for information about a service center close to you.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How do I claim warranty if the warranty card is not sealed on the product I bought.?</a>
                  <ul>
                    <li class=""> Most service centres accept a proof of purchase (i.e. original invoice) to provide warranty service. In case of any grievances contact customer support. </li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Why is the warranty card on my product not sealed?</a>
                  <ul>
                    <li class="">
                      	 	To seal the warranty card, we have to open the box and as customers do not accept open packages that’s why the warranty cards aren’t sealed. Please keep proof of purchase safe to claim warranty.</li>
                  </ul>
                </li>
                <li class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What is the warranty for the replacement I got? Will it be the same as the initial product?</a>
                  <ul>
                    <li class="">  	Yes, the warranty for your replacement will be the same as your initial product. </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>  
        
        </div>
        
        <div class="col-sm-4  ">
        	<div class="filter-options card mb20">
         <h4>Frequently Asked Questions</h4>
          <div class="filteroption"   >
            <div class="categoriesfilter">
              <ul>
                <li  class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How do I track my order?</a>
                  <ul>
                    <li class=""> An SMS with waybill no. will be sent to your registered phone number. Please check the courier company's website with the waybill number to track your order. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >I missed the delivery of my order today how do I get it delivered again?</a>
                  <ul>
                    <li class=""> We will call you to confirm your availability and reschedule the delivery for you. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How long would it take to get my order delivered?</a>
                  <ul>
                    <li class=""> Most orders take around 7 business days to get processed. A few items and some locations require slightly longer due to logistics issues. Check your order details in My Orders. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Can package be accepted after checking the contents inside it?</a>
                  <ul>
                    <li class=""> A package can’t be opened before delivery is accepted as per our company policy. But you may accept the package & get in touch with us later in case you have any concerns. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What should I do in case my order is delayed?</a>
                  <ul>
                    <li class=""> It rarely happens that an order gets delayed, nevertheless you can check your email and messages for an updated delivery timeline OR you can call the support number and speak to one of our account managers. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What should I do if my order is approved but not yet dispatched for a long time?</a>
                  <ul>
                    <li class=""> Please help us look into it by contacting our Customer Support. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Will the delivery be re-attempted if I’m not able to collect my order the first time?</a>
                  <ul>
                    <li class=""> Yes, we’ll make sure that the delivery is re-attempted the next working day if you can’t collect your order the first time. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Can I change the time of delivery for my order?</a>
                  <ul>
                    <li class=""> Sorry, time of delivery for an order cannot be changed. </li>
                  </ul>
                </li>
                <li   class="closed"> <a href="javascript:void(0)" class="subdropdown headingcat "  > The package I received was broken and the product inside damaged at the time of delivery. What should I do?</a>
                  <ul>
                    <li class="">•	You can request for the item to be replaced or returned by using our return feature.</li>
                    <li class="">•	The replacement will be delivered to you in the next 5-7 days or if you choose to return the consignment, you will be offered a full refund. </li>
                  </ul>
                </li>
          
              </ul>
            </div>
          </div>
          
          
        </div>
        <div class="card">
          	<h5>Issue still not resolved?</h5>
            <a class="btn lightgraybtn">Contact us</a>
          </div>
        </div>
    </div>
    
    
      
    </div>
  </div>
</div>
<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>
