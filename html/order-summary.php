<div class="col-md-3 col-sm-12 order-part padding-mobile" id="orderSummary">
	<div class="theiaStickySidebar">
    	<div class="mb30 box-heading-inner-order">
        	<table class="coupon table table-responsive" width="100%">
                <tr>
                    <td width="80%">
                        <h4>Redeem Points - <span class="price greentext"><span class="rupee">₹</span> 2,400</span></h4>
                        <span class="msg graytext">(Max. 10% of cart value)</span>
                    </td>
                    <td width="20%" align="right">
                        <button class="btn lightgraybtn">Redeem</button>
                    </td>
                </tr>
                <tr>
                    <td width="80%">
                        <input type="text" name="txt_frm_no" placeholder="Enter Coupon Code" class="form-control">
                    </td>
                    <td width="20%" align="right">
                        <button class="btn lightgraybtn">Apply</button>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="2">
                        <div class="alert alert-success">
                          	<a href="#" class="close redtext" data-dismiss="alert" aria-label="close">&times;</a>
                          	<strong>AMEDG23000</strong> 
                        </div>
                        <span class="msg greentext">Coupon code successfully applied</span>
                    </td>
                </tr>
                <!--<tr>
                    <td width="100%" colspan="2">
                        <span class="msg greentext">Coupon code successfully applied</span>
                    </td>
                </tr>-->
          	</table>
        </div>
        <h3 class="mb30">Order Summary</h3>
        <div class="box-heading-inner-order">
            <table class="table table-responsive" width="100%">
                <tr>
                    <td width="60%">
                        <h4>Product Total</h4>
                    </td>
                    <td width="40%" align="right">
                        <h4 class="graytext"><span class="rupee">₹</span> 2,00,000</h4>
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        <h4>Shipping Charge</h4>
                    </td>
                    <td width="40%" align="right">
                        <h4 class="graytext"><span class="rupee">₹</span> 1,000</h4>
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        <h4>VAT/CST</h4>
                    </td>
                    <td width="40%" align="right">
                        <h4 class="graytext">- <span class="rupee">₹</span> 5,000</h4>
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        <h4>Service Tax</h4>
                    </td>
                    <td width="40%" align="right">
                        <h4 class="graytext">- <span class="rupee">₹</span> 200</h4>
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        <h4><span class="red">Redeem</span></h4>
                    </td>
                    <td width="40%" align="right">
                        <h4><span class="red">- <span class="rupee">₹</span> 1,000</span></h4>
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        <h4><span class="red">Coupon Discount</span></h4>
                    </td>
                    <td width="40%" align="right">
                        <h4><span class="red">- <span class="rupee">₹</span> 10,000</span></h4>
                    </td>
                </tr>
            </table>
            <table class="table table-striped" width="100%">
                <tr>
                    <td width="50%">
                        <h3>TOTAL</h3>
                    </td>
                    <td width="50%" align="right">
                        <h3><span class="rupee">₹</span> 1,91,000</h3>
                    </td>
                </tr>
            </table>
        </div>
        <div class="help-box1 mb30">
        	<table class="table" width="100%">
                <tr>
                    <td width="100%">
                        <label class="mt10"><input type="checkbox" class="checkbox-inline" checked>I agree to <a href="#">Terms & Conditions</a></label>
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <button class="btn2 orangebtn">PROCEED TO PAYMENT</button>
                        <!--<button class="btn2 darkgraybtn">APPLY FOR BANK CREDIT</button>-->
                    </td>
                </tr>
            </table>
        </div>
        <div class="help-box mb30">
            <h4>Need Help?</h4>
            <hr>
            <p><i class="glyphicon glyphicon-earphone"></i> +91 98603 00772 (24x7)</p>
            <p><i class="glyphicon glyphicon-envelope"></i> <a href="#">support@medibridge.com</a></p>
        </div>
        <!--<div class="help-box">
            <h4>Policies</h4>
            <hr>
            <ul>
            	<li><a href="#">Shipping Policy</a></li>
                <li><a href="#">Return Policy</a></li>
                <li><a href="#">Cancellation Policy</a></li>
                <li><a href="#">Terms & Conditions</a></li>
            </ul>
        </div>-->
 	</div>
</div>