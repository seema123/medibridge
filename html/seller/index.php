<?php
include 'header.php';
?>
<div class="nav-wrap navbarseller">
<div class="container">
 <ul class="group example-one" id="example-one"   >
    <li class="current_page_item"><a href="index.php">Home</a></li>
    <li><a href="seller-advantage.php">MediBridge Advantage</a></li>
    <li><a href="seller-faq.php">FAQ</a></li>
 </ul>
 </div>
</div>
<div class="container relative">
	
  <div class="sellerbanner">
    <div class="owl-carousel owl-theme">
      <div class="item"> <img src="../images/seller/banner/img1.jpg" class="img-responsive" ></div>
      <div class="item"> <img src="../images/seller/banner/img1.jpg" class="img-responsive" ></div>
    </div>
  </div>
  <!--Seller form start-->
  <div class="sellerform">
    <h3>Register Now</h3>
    <div class="form-group">
      <input type="email" class="form-control" placeholder="Company Name">
    </div>
    <ul class="form-group formli">
      <li class="">
        <input type="email" class="form-control" placeholder="Email ID">
      </li>
      <li class="">
        <input type="password" class="form-control" placeholder="Password">
      </li>
    </ul>
    <ul class="form-group formli">
      <li class="">
        <input type="text" class="form-control" placeholder="Mobile Number">
      </li>
      <li class="">
        <input type="text" class="form-control"  placeholder="Address">
      </li>
    </ul>
    <ul class="form-group formli">
      <li class="">
        <input type="text" class="form-control" placeholder="Company PAN number">
      </li>
    </ul>
    <div class="text-center"> 
    <button type="submit" class="btn orangebtn">SELL NOW</button></div>
  </div>
	<!--Seller form end-->
  <div class="boxwrap">
    <div class="row">
      <div class="col-sm-4">
        <div class="s-boxwrap"> <img src="../images/seller/india-icn.jpg"  >
          <p>Sell to and from Any location in the country</p>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="s-boxwrap"> <img src="../images/seller/clock.jpg"  >
          <p>Sell 24X7</p>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="s-boxwrap"> <img src="../images/seller/payment.jpg"  >
          <p>Easy, quick and transparent Payment methods </p>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="easystepwrap">
  <div class="container">
    <h2>3 easy steps to boost your business online</h2>
    
     <div class="steps">
     	<div class="stepswrap">
     		<div class="stepsbg">1</div>
     		<div class="stepsbox media">
      <div class="media-left"> <img src="../images/seller/register.png" ></div>
      <div class="media-body">
        <h3>Register</h3>
        <ul class="list-grayArrow">
          <li>Register and create your free account with Medeness.com.</li>
          <li>Fill in the details of your business and your offerings.</li>
        </ul>
      </div>
    </div>
     	</div>
        <div class="stepswrap">
     		<div class="stepsbg">2</div>
    <div class="stepsbox media">
      <div class="media-left"> <img src="../images/seller/cart.png"  ></div>
      <div class="media-body">
        <h3>Catalogue and start selling</h3>
        <ul class="list-grayArrow">
          <li>Get your products catalogued with the help of our accessory service professionals.</li>
          <li>Your listed products will be accessible by millions of consumers across India.</li>
          <li>Get orders and start selling.</li>
        </ul>
      </div>
    </div>
    	</div>
        <div class="stepswrap">
     		<div class="stepsbg">3</div>
    <div class="stepsbox media">
      <div class="media-left"> <img src="../images/seller/ship.png"  ></div>
      <div class="media-body">
        <h3>Ship and get payed</h3>
        <ul class="list-grayArrow">
          <li>Once and order is received we take care of the logistics from pick-up to delivery.</li>
          <li>Get quick and hassle-free payments once the order is fulfilled </li>
        </ul>
      </div>
    </div>
    	</div>
     </div>
     
     
    
    
    
    
  </div>
</div>


<!-- -->

<div class="businessstart">
<div class="container">
	<div class="row">
    <div class="col-md-4 col-sm-6">
      <div class="business">Start your business with clinito & reach customers across India</div>
    </div>
    <div class="col-md-4 nodisplayInIpad"></div>
    <div class="col-md-4 col-sm-6"> <a class="startDiv orangebtn btn largebtn"  href="#top">START SELLING NOW</a> </div>
  </div>
</div>
</div>

<div class="seofooter">
	<div class="container">
    <div class="row">
    <div class="col-md-12">
<p>Clinito’s vision is to create India’s most impactful digital commerce ecosystem that creates life-changing experiences for buyers and sellers. </p>
    <p>In its endeavour to make online business easy, Clinito has pioneered with a host of industry-first initiatives to help its sellers grow and prosper. Clinito provides expert assistance through its Advisors who help sellers manage their online business. Sellers also get free training &amp; support and all the information they need for carrying out their business. Clinito has also partnered with leading banks and runs a programme called Capital Assist that helps sellers fund their growth by getting unsecured loans at low interest for their working capital requirements. Clinito also provides sellers with free inventory storage and faster, hassle free deliveries through a service called Clinito Plus. Clinito also helps sellers get in touch with professionals and experts for their various requirements such as Onboarding, Cataloguing, Photography, Business Consulting, etc.</p>
<p>
    <b>
        *India map imagery as a pictorial representation does not purport to be the political map of India
    </b>
</p>
    </div>
    </div></div>
</div>



<?php
include 'footer.php';
?>