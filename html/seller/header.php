<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Medibridge</title>

<!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/owl.carousel.css" rel="stylesheet">
<link href="../css/jquery.fancybox.css" rel="stylesheet">
<link href="../css/bootstrap.vertical-tabs.css" rel="stylesheet">
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/style_seller.css" rel="stylesheet">
<link href="../css/style_g.css" rel="stylesheet">
<link href="../css/style-res.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="sellerwrap">
<!----> 
<!-- Modified navbar: Animating from right to left (off canvas) -->
<nav id="navbar2" class="navbar navbar-default visible-xs" role="navigation"> 
  
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button class="btn4 btn-clean" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3"> <span class="glyphicon glyphicon-menu-hamburger"></span> </button>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
    <div class="backbtn">
      <button class="btn4 btn-link" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3"> <span class="glyphicon glyphicon-chevron-left"></span> BACK </button>
    </div>
    <div class="navigation">
      <div class="filter-options">
        <h4 class="tname"><i class="glyphicon glyphicon-log-in"></i> LOGIN </h4>
      </div>
      <div class="filter-options">
        <h4 class="tname"><i class="glyphicon glyphicon-user"></i> MY ACCOUNT<span class="sprite"></span></h4>
        <div class="filteroption">
          <div class="categoriesfilter">
            <ul class="vscroll">
              <li  class="closed" > <a href="javascript:void(0)" class="subdropdown headingcat">Lab Supplies</a>
                <ul>
                  <li class=""> <a href="" class="">Beakers</a> </li>
                  <li class=""><a href="" >Blood Collection</a></li>
                  <li class=""><a href="" >Caseettes</a></li>
                </ul>
              </li>
              <li class="closed" > <a href="javascript:void(0)" class="subdropdown headingcat "  >Anesthesia</a>
                <ul>
                  <li class="closed"><a href="javascript:void(0)" class="subdropdown headingcat " >Blood Collection</a>
                    <ul>
                      <li class=""><a href="" >Beakers</a></li>
                      <li class=""><a href="" >Blood Collection</a></li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                  <li class=""><a href="" >Caseettes</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="filter-options">
        <h4 class="tname  ">Shop By Category<span class="sprite"></span></h4>
        <div class="filteroption">
          <div class="categoriesfilter">
            <ul class="vscroll">
              <li  class="closed" > <a href="javascript:void(0)" class="subdropdown headingcat">Lab Supplies</a>
                <ul>
                  <li class=""> <a href="" class="">Beakers</a> </li>
                  <li class=""><a href="" >Blood Collection</a></li>
                  <li class=""><a href="" >Caseettes</a></li>
                </ul>
              </li>
              <li class="closed" > <a href="javascript:void(0)" class="subdropdown headingcat "  >Anesthesia</a>
                <ul>
                  <li class="closed"><a href="javascript:void(0)" class="subdropdown headingcat " >Blood Collection</a>
                    <ul>
                      <li class=""><a href="" >Beakers</a></li>
                      <li class=""><a href="" >Blood Collection</a></li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                  <li class=""><a href="" >Caseettes</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="filter-options">
        <h4 class="tname">Shopping List </h4>
      </div>
      <div class="filter-options">
        <h4 class="tname">LOGIN </h4>
      </div>
    </div>
  </div>
  <!-- /.navbar-collapse --> 
  
  <!-- /.container-fluid --> 
</nav>
<div class="header-container">
  <header id="topNavmain">
    <div class="container">
      <div class="row"> 
        <!-- Logo -->
        <div class="col-sm-3 col-xs-8 ">
          <div class="logo"> <a href="index.php"><img src="../images/Clinito_Final_Logo.svg" class="img-responsive"> </a> </div>
        </div>
        <!-- End Logo --> 
        <!-- Search Form -->
        <div class="col-sm-9 hidden-xs ">
          <div class="sellerlogin">
            <form class="form-inline">
              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail3">Email ID</label>
                <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
              </div>
              <div class="form-group">
                <label class="sr-only" for="exampleInputPassword3">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
              </div>
              <button type="submit" class="btn orangebtn">Log in</button>
            </form>
          </div>
        </div>
        <!-- End Shopping Cart List --> 
      </div>
    </div>
  </header>
  <div class="sellerlogin visble-xs hidden">
    <form class="form-inline">
      <div class="form-group">
        <label class="sr-only" for="exampleInputEmail3">Email ID</label>
        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
      </div>
      <div class="form-group">
        <label class="sr-only" for="exampleInputPassword3">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
      </div>
      <button type="submit" class="btn orangebtn">Log in</button>
    </form>
  </div>
</div>
