<!--footer start-->

<footer>
  <div class="container">
    <div class="row footer-top">
      <div class="col-sm-7">
        <h4 class="footerheading">Contact Us</h4>
        <ul class="fLinks">
          <li><span class="sprite flocation"></span>201, 2nd Floor, Gagangiri, Mumbai, 400055</li>
          <li><span class="sprite fcall"></span>Toll free: 1800-300-23456 (9 AM - 8 PM)</li>
          <li><span class="sprite fmail"></span><a href="mailto:care@medibridge.com">care@medibridge.com</a></li>
        </ul>
      </div>
      <div class="col-sm-5">
        <h4 class="footerheading">Subscription</h4>
        <div class="subscriptionhld">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Please enter your email">
            <span class="input-group-btn">
            <button class="btn" type="button">Subscribe</button>
            </span> </div>
          <!-- /input-group -->
          <p class="mtb10">Register now to get updates on promotions and coupons.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <h4 class="footerheading">Follow Us</h4>
        <ul class="social-icons">
          <li><a href="" class="gmailicn sprite"></a></li>
          <li><a href="" class="facebbokicn sprite"></a></li>
          <li><a href="" class="twitericn sprite"></a></li>
          <li><a href="" class="instaicn sprite"></a></li>
        </ul>
      </div>
      <div class="col-sm-6">
        <div class="copyrighttext">© Copyright 2016. Healthcare Advisor.. All Rights Reserved.</div>
      </div>
    </div>
  </div>
</footer>
<a href="#" class="scrollup" style=""></a> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="../js/bootstrap.min.js"></script> 
<!--<script src="../js/bootstrap-hover-dropdown.min"></script>--> 
<script src="../js/owl.carousel.min.js"></script> 
<!--<script src="http://www.menucool.com/vertical/vm/menu-vertical.js" type="text/javascript"></script>--> 
<!--filter price range--> 
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 
<!--<script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script> --> 
<script src="../js/jquery.ez-plus.js"></script> 
<!--input field--> 
<script src="../js/classie.js"></script> 
<!--zoom effect --> 
<!--<script src="../js/jquery.elevatezoom.js"></script> --> 
<!--filter scroll--> 
<script src="../js/jquery.slimscroll.min.js"></script> 


<!-- Add fancyBox main JS and CSS files --> 
<script type="text/javascript" src="../js/jquery.fancybox.pack.js"></script> 
<script src="../js/seller-js.js"></script>

 
</body></html>