<?php
include 'header.php';
?>
<div class="nav-wrap navbarseller">
  <div class="container">
    <ul class="group example-one" id="example-one"   >
      <li><a href="index.php">Home</a></li>
      <li class="current_page_item"><a href="seller-advantage.php">MediBridge Advantage</a></li>
      <li><a href="seller-faq.php">FAQ</a></li>
    </ul>
  </div>
</div>
<div class="sellertab">
  <div class="container"> 
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active col-sm-4"><a href="#growthsec" aria-controls="growthsec" role="tab" data-toggle="tab">
        <h3>Growth</h3>
        <p>Expand the horizons for your business </p>
        </a></li>
      <li role="presentation" class=" col-sm-4"><a href="#easeicn" aria-controls="easeicn" role="tab" data-toggle="tab">
        <h3>Ease</h3>
        <p>Conduct your business with ease. </p>
        </a></li>
      <li role="presentation" class=" col-sm-4"><a href="#dependableicn" aria-controls="dependableicn" role="tab" data-toggle="tab">
        <h3>Dependable</h3>
        <p>Complete transparency </p>
        </a></li>
    </ul>
  </div>
</div>
<div class="sellertabcontain">
  <div class="container">
    <div class="row"> 
      <!-- Tab panes -->
      <div class="tab-content ">
        <div role="tabpanel" class="tab-pane active" id="growthsec">
          <div class="row">
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/capitalicn.jpg" ></div>
                <div class="media-body">
                  <h3>Sourcing capital</h3>
                  <p>Get your businesses easily funded. Get loans processed effectively from approval to
                    disbursement within 72 hours. </p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/recommand-icn.jpg" ></div>
                <div class="media-body">
                  <h3>Trends and recommendations </h3>
                  <p>Get insights on latest business trends and recommendations by our advisors to help
                    you grow your business. </p>
                </div>
              </div>
            </div>
            </div>
            <div class="row">
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/clocl.jpg" ></div>
                <div class="media-body">
                  <h3>24X7 selling portal </h3>
                  <p>Tap into the online market completely and cover a broader spectrum of consumers. </p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/marketingicn.jpg" ></div>
                <div class="media-body">
                  <h3>Marketing assistance</h3>
                  <p>We help you highlight your products by running highly effective marketing campaigns. </p>
                </div>
              </div>
            </div>
            </div>
            <div class="row">
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/advertisement-icn.jpg" ></div>
                <div class="media-body">
                  <h3>Advertisements</h3>
                  <p>Showcase your products with Medibridge
                    ads to guarantee visibility to escalate
                    your sales. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--growthsec-->
        <div role="tabpanel" class="tab-pane" id="easeicn"> 
        	<div class="row">
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/madness-icn.jpg" ></div>
                <div class="media-body">
                  <h3>Medibridge Advice</h3>
                  <p>Online businesses demand a sound understanding of the online world. Our advisors will help you on every step to ensure that you do well with your online presence.
 </p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/userfriendly.jpg" ></div>
                <div class="media-body">
                  <h3>User-friendly seller panel  </h3>
                  <p>Easy to use sellers panel gives you transparency and control over your business.
 </p>
                </div>
              </div>
            </div>
             </div>
             <div class="row">
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/training-icn.jpg" ></div>
                <div class="media-body">
                  <h3>Free training </h3>
                  <p>Get free online training session to educate yourself with various processes and be ahead of the curve. </p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/service-icn.jpg" ></div>
                <div class="media-body">
                  <h3>Service accessories</h3>
                  <p>Get access to services of various professionals for tasks like photoshoots, cataloguing etc. </p>
                </div>
              </div>
            </div>
            </div>
            <div class="row">
            <div class="col-sm-6">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/recommand-icn.jpg" ></div>
                <div class="media-body">
                  <h3>Logistics assistance</h3>
                  <p>Get the most of our logistics services as we undertake everything from pick-up, storage and delivery across the country.  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--easeicn-->
        <div role="tabpanel" class="tab-pane" id="dependableicn"> 
        
         <div class="row">
            <div class="col-sm-12">
              <div class="media advantagebox">
                <div class="media-left"><img src="../images/seller/transparency.jpg" ></div>
                <div class="media-body text-left">
                  <h3>Complete transparency </h3>
                  <p>Be in charge of your business by having complete transparency and control over every aspect of your business. </p>
                </div>
              </div>
            </div>
         </div>
         
         <div class="chartseller">
         <h3>Review your sales</h3>
         <p>Get a detailed description of all your sales and keep track of all the financials involved.</p>
        <img src="../images/seller/chart1.jpg"  class="img-responsive" > 
        </div>
        
        <div class="chartseller">
         <h3>Review your orders</h3>
         <p>Keep yourself updated with information and statuses of all your orders, old and new.</p>
        <img src="../images/seller/chart2.jpg"  class="img-responsive" > 
        </div>
        </div>
        <!--dependableicn--> 
      </div>
    </div>
  </div>
</div>


<!-- -->

<div class="businessstart">
<div class="container">
	<div class="row">
    <div class="col-md-4 col-sm-6">
      <div class="business">Start your business with clinito & reach customers across India</div>
    </div>
    <div class="col-md-4 nodisplayInIpad"></div>
    <div class="col-md-4 col-sm-6"> <a class="startDiv orangebtn btn largebtn"  href="#top">START SELLING NOW</a> </div>
  </div>
</div>
</div>

<div class="seofooter">
	<div class="container">
    <div class="row">
    <div class="col-md-12">
<p>Clinito’s vision is to create India’s most impactful digital commerce ecosystem that creates life-changing experiences for buyers and sellers. </p>
    <p>In its endeavour to make online business easy, Clinito has pioneered with a host of industry-first initiatives to help its sellers grow and prosper. Clinito provides expert assistance through its Advisors who help sellers manage their online business. Sellers also get free training &amp; support and all the information they need for carrying out their business. Clinito has also partnered with leading banks and runs a programme called Capital Assist that helps sellers fund their growth by getting unsecured loans at low interest for their working capital requirements. Clinito also provides sellers with free inventory storage and faster, hassle free deliveries through a service called Clinito Plus. Clinito also helps sellers get in touch with professionals and experts for their various requirements such as Onboarding, Cataloguing, Photography, Business Consulting, etc.</p>
<p>
    <b>
        *India map imagery as a pictorial representation does not purport to be the political map of India
    </b>
</p>
    </div>
    </div></div>
</div>


<?php
include 'footer.php';
?>