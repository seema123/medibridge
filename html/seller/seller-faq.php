<?php
include 'header.php';
?>
<div class="nav-wrap navbarseller">
<div class="container">
 <ul class="group example-one" id="example-one"   >
    <li><a href="index.php">Home</a></li>
    <li><a href="seller-advantage.php">MediBridge Advantage</a></li>
    <li  class="current_page_item"><a href="seller-faq.php">FAQ</a></li>
 </ul>
 </div>
</div>
<!--bredcrumbs-->
 
<div class="container relative">
  <div class=" innercontent faqwrap">
    <h1 class="titleh">Frequently Asked Questions </h1>
    <div class="p20 bgwhite">
      <div class="row">
        <div class="col-sm-8">
          <form class="form ">
            <div class="input-group text-center mb5">
              <input type="text" class="form-control input-lg" placeholder="What can we help with?">
              <span class="input-group-btn">
              <button class="btn btn-lg bluebtn" type="button">Search</button>
              </span> </div>
          </form>
          <p class="help-text mb20 graytext font12" >ex : Returns, Exchange, Damaged product</p>
          <div class="faqhld">
            <div class="filter-options">
              <h4 class="tname"><i class="faqordericn"></i>Getting Started<span class="sprite"></span></h4>
              <div class="filteroption">
                <div class="categoriesfilter">
                  <ul>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Why should I sell with CLINITO? 4-5</a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt>	Access to a large group of potential customers across the Indian Subcontinent without any territorial limitation.</dt>
                            <dt>	Payments for services rendered made on a timely basis.</dt>
                            <dt>	Logistics charges included as a part of the service fees.</dt>
                            <dt>	Increased visibility through online presence and advertising</dt>
                            <dt>	Benefits from analytics and purchase patterns across categories and geographies</dt>
                            <dt>	No registration fees. </dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How do I register myself as a seller on CLINITO.com? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt> Contact one of our sales personnel (Details to be provided on site).</dt>
                            <dt>	Submit all necessary KYC vendor documentation and sign relevant agreements.</dt>
                            <dt>	Once all documentation is validated, Client Services personnel will contact you to assist with account creation and explanation of the features available.</dt>
                            <dt>	You are now registered and ready to transact on CLINITO and will be contacted by your assigned account manager</dt>
                            <br>
                            OR<br>
                            <br>
                            <dt>	Click on the following link < link to be provided></dt>
                            <dt>	Fill in all your company details and click on “Sell Now”</dt>
                            <dt>	Download, review, sign and upload the Supplier Agreement & all other relevant documentation.</dt>
                            <dt>	Once all the documentation is validated, the seller will be registered and ready to transact and will be contacted by your assigned account manager.</dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What are the charges of Sell on CLINITO program? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt>	There is an annual cost of Re. 2500/- associated with registering the seller’s pick up facility/facilities.</dt>
                            <dt>	There are no product listing fees and no other registration fees</dt>
                            <dt>	CLINITO will take a service fee on all transactions taking place on the portal. This fee will be inclusive of all logistics costs.</dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Who can sell on CLINITO.com?</a>
                      <ul>
                        <li class=""> Any authorized vendor/distributor or manufacturer selling medical consumables, supplies, instrumentation, devices, equipment and other categories within our scope of products. </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What products can I sell on CLINITO.com? </a>
                      <ul>
                        <li class=""> Any products that fall within the scope of our catalogue of materials (Refer to “Catalogue sheet”) </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > I don't have a website; can I still sell on CLINITO.com? </a>
                      <ul>
                        <li class=""> Yes, you do not require a website or any online presence. Simply list your products with us and you are ready to transact with our customers. </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="filter-options closed">
              <h4 class="tname closed "><i class="faqcanicn"></i>Onboarding<span class="sprite"></span></h4>
              <div class="filteroption" style="display:none">
                <div class="categoriesfilter">
                  <ul>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How does selling on CLINITO.com work? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt>	Once all the submitted documentation is validated, the client services team will guide you with regards to navigating the portal. You will be able to list the products you wish to out of your catalogue, add new products as and when you choose and modify pricing when necessary.</dt>
                            <dt>	Once a buyer places an order, you will be notified by SMS and email. </dt>
                            <dt>	You can then confirm the order by changing the status of the order to “ready for shipping”.</dt>
                            <dt>	If you change the status to “Out of Stock”, CLINITO will receive a notification and we will adjust the order against another supplier.</dt>
                            <dt>	Logistics will then pick up your order from your location and deliver it to the buyer, and you will be notified on both occasions. </dt>
                            <dt>	CLINITO will then receive transfer the payment of the order on the date previously agreed upon. </dt>
                          </dl>
                        </li>

                      </ul>
                    </li>
                        <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What is the guarantee that I will get business? </a>
                          <ul>
                            <li class="">
                              <dl>
                                <dt>	We have an active business development team that is reaching out to the widest possible market in the Indian healthcare space </dt>
                                <dt>	Our marketing and sales efforts will be conducted both digitally and on ground </dt>
                                <dt>	We will target specific medical groups concurrent to the products listed </dt>
                                <dt>	We will also shift our current existing network of healthcare providers online to avail of the products you will list on the site.</dt>
                                <dt>	If you still feel there is less to no business being directed your way, speak to your assigned account manager for detailed market analytics that could shed some light on possible causes and solutions. </dt>
                              </dl>
                            </li>
                          </ul>
                        </li>
                        <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > What documents I require to start selling on CLINITO? </a>
                          <ul>
                            <li class="">
                              <dl>
                                <dt>Once you register your intent to work with us, we shall email you a list of documents necessary to onboard you. </dt>
                                <dt>Please provide these documents at the earliest.</dt>
                              </dl>
                            </li>
                          </ul>
                        </li>
                        <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How do I list my products on CLINITO.com? </a>
                          <ul>
                            <li class="">
                              <dl>
                                <dt> Once you are on boarded, you will be provided your very own account page where you will be able to add/edit products at your convenience. </dt>
                                <dt> With just one click, add products to your catalogue and add price </dt>
                                <dt> Certain products that are not in our catalogue may have to be added individually. Please contact our client services team if you encounter any difficulties or queries. </dt>
                              </dl>
                            </li>
                          </ul>
                        </li>
                        <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Can CLINITO help in capturing the images for the catalogue? </a>
                          <ul>
                            <li class="">
                              <dl>
                                <dt> </dt>
                              </dl>
                            </li>
                          </ul>
                        </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="filter-options closed">
              <h4 class="tname closed"><i class="faqpaymenticn"></i>Selling <span class="sprite"></span></h4>
              <div class="filteroption" style="display:none">
                <div class="categoriesfilter">
                  <ul>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > When can I start selling? </a>
                      <ul>
                        <li class=""> Once all the vendor documentation is validated, your account will be activated and our client services team will guide you for conducting business. </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How will I get Paid? What is the payment cycle? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt> Payment will be conducted on a (weekly,bi-weekly, monthly) period. </dt>
                            <dt> All dues owed will be paid in full.</dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > How do I manage the Orders? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt> There is a separate tab which allows suppliers to track orders as well as manage and update his/her stock accordingly. </dt>
                            <dt> <i> For every new order, there will be a notification sent to the supplier via SMS and email. </i></dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Can I sell outside India through CLINITO.com? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt> Yes you may, but CLINITO will not bear the logistics costs on those purchases.</dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Can I cancel my subscription? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt> Yes you may, but please provide us a reason, so that we may improve our service and return to you with a better arrangement in the future.</dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                    <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > If I list my products on CLINITO.com, will the customer know that he or she is purchasing from me on CLINITO.com? </a>
                      <ul>
                        <li class="">
                          <dl>
                            <dt> When you are listed on CLINITO, you will be provided a Dynamic code that will keep your name anonymous. </dt>
                            <dt> While shipping the product, the invoice will be raised by the suppliers and bearing his/her name or details. </dt>
                            <dt> We do this in the interest of fair trade in the market place and to avoid any show of bias towards any particular sellers. </dt>
                          </dl>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4  ">
          <div class="filter-options card mb20">
            <h4>Frequently Asked Questions</h4>
            <div class="filteroption"   >
              <div class="categoriesfilter">
                <ul>
                  <li  class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How do I track my order?</a>
                    <ul>
                      <li class=""> An SMS with waybill no. will be sent to your registered phone number. Please check the courier company's website with the waybill number to track your order. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  >I missed the delivery of my order today how do I get it delivered again?</a>
                    <ul>
                      <li class=""> We will call you to confirm your availability and reschedule the delivery for you. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  >How long would it take to get my order delivered?</a>
                    <ul>
                      <li class=""> Most orders take around 7 business days to get processed. A few items and some locations require slightly longer due to logistics issues. Check your order details in My Orders. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Can package be accepted after checking the contents inside it?</a>
                    <ul>
                      <li class=""> A package can’t be opened before delivery is accepted as per our company policy. But you may accept the package & get in touch with us later in case you have any concerns. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What should I do in case my order is delayed?</a>
                    <ul>
                      <li class=""> It rarely happens that an order gets delayed, nevertheless you can check your email and messages for an updated delivery timeline OR you can call the support number and speak to one of our account managers. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  >What should I do if my order is approved but not yet dispatched for a long time?</a>
                    <ul>
                      <li class=""> Please help us look into it by contacting our Customer Support. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  >Will the delivery be re-attempted if I’m not able to collect my order the first time?</a>
                    <ul>
                      <li class=""> Yes, we’ll make sure that the delivery is re-attempted the next working day if you can’t collect your order the first time. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > Can I change the time of delivery for my order?</a>
                    <ul>
                      <li class=""> Sorry, time of delivery for an order cannot be changed. </li>
                    </ul>
                  </li>
                  <li   class="closed sublink"> <a href="javascript:void(0)" class="subdropdown headingcat "  > The package I received was broken and the product inside damaged at the time of delivery. What should I do?</a>
                    <ul>
                      <li class="">	You can request for the item to be replaced or returned by using our return feature.</li>
                      <li class="">	The replacement will be delivered to you in the next 5-7 days or if you choose to return the consignment, you will be offered a full refund. </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <h5>Issue still not resolved?</h5>
            <a class="btn lightgraybtn">Contact us</a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include '../footer-top.php';
?>
<?php
include 'footer.php';
?>
