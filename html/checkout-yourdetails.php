<?php include 'header.php'; ?>
<div class="container" id="checkoutBox">
	<div class="row">
		<div class="col-md-12">
			<div class="row nav-box">
				<div class="col-md-12">
					<ul class="checkoutNav">
						<li class="active">
							<a href="checkout-shoppingcart.php"><h4><span class="iconbox">1</span> Shopping Cart</h4></a>
						</li>
						<li><img src="images/nav-arrow.png"></li>
						<li class="active">
							<a href="checkout-yourdetails.php"><h4><span class="iconbox">2</span> Your Details</h4></a>
						</li>
						<li><img src="images/nav-arrow.png"></li>
						<li>
							<a href="checkout-paymethod.php"><h4><span class="iconbox">3</span> Payment Methods</h4></a>
						</li>
						<li><img src="images/nav-arrow.png"></li>
						<li>
							<a href="checkout-confirmation.php"><h4><span class="iconbox">4</span> Comfirmation</h4></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row nav-box-second">
				<div class="col-md-4 col-sm-4 detail-box">
					<h4>Your Details</h4>
					<div class="details">
						<p class="namehead">Nitin Kawale</p>
						<p>+91 98765 43210</p>
						<p>nitin@gmail.com</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 detail-box">
					<h4>Billing Address <span class="pull-right"><a href="#">Add another</a></span></h4>
					<div class="details">
						<div class="editDeleteBlock">
							<ul>
								<li>
									<a href="#"><img src="images/edit.png"></a>
								</li>
							</ul>
						</div>
						<p class="namehead">Nitin Kawale</p>
						<p>203, Block C, Sunmil Compound, Lower Parel, Mumbai - 400 034.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 detail-box">
					<h4>Shipping Address <span class="pull-right"><a href="#">Add another</a></span></h4>
					<div class="details">
						<div class="editDeleteBlock">
							<ul>
								<li>
									<a href="#"><img src="images/edit.png"></a>
								</li>
							</ul>
						</div>
						<p class="namehead">Nitin Kawale</p>
						<p>203, Block C, Sunmil Compound, Lower Parel, Mumbai - 400 034.</p>
					</div>
					<label><input type="checkbox" class="checkbox-inline" checked> Same as billing address</label>
				</div>
			</div>
			<div class="row nav-box-second">
				<div class="col-lg-12 col-md-12 col-sm-12 padding-mobile">
					<div class="box-heading-details">
						<div class="col-md-4 col-sm-4 text-left">
							<p><strong>Do you want multiple drop delivery?</strong></p>
						</div>
						<div class="col-md-2 col-sm-2 text-left block-label">
							<label><input type="radio" name="multipledrop" value="Yes" class="checkbox-inline" > Yes</label>
							<label><input type="radio" name="multipledrop" value="No" class="checkbox-inline" checked>No</label>
						</div>
						<div class="col-md-5 col-sm-5 text-left">
							<table width="100%" class="mt10">  
								<tr>
                                <td><span class="grey ">(Note: For multiple drop delivery charges will be 2% off card value <span class="rupee"> ₹</span> 100)</span></td>
									<!--<td width="40%"><button class="btn lightgraybtn">Submit</button></td>-->
									
								</tr>
							</table>
						</div>
					</div>
				</div>
                <div class="text-center hidden"><button class="btn  orangebtn">PROCEED TO PAYMENT</button></div> 
				<div class="col-md-9 col-sm-12 padding-mobile">
					<div class="box-heading hidden-xs">
						<div class="item-image">
							&nbsp;
						</div>
						<div class="item-name">
							<h4>Item</h4>
						</div>
						<div class="item-qty">
							<h4>Qty</h4>
						</div>
						<!--<div class="item-date">
							<h4>Date Placed</h4>
						</div>-->
						<div class="item-b_add">
							<h4>Billing<br /> Address</h4>
						</div>
						<div class="item-s_add">
							<h4>Shipping<br /> Address</h4>
						</div>
						<div class="item-dev_qty">
							<h4>Delivery<br /> Qty</h4>
						</div>
						<!--<div class="item-ship_charge">
							<h4>Shipping<br /> Charges</h4>
						</div>-->
						<div class="item-close">
							&nbsp;
						</div>
					</div>
					<div class="box-heading-inner">
						<div class="firstBlock">
							<div class="item-image">
								<a href="#"><img src="images/product.jpg"></a>
							</div>
							<div class="item-name">
								<h5><a href="#">Low scale beaker by kimberly</a></h5>
								<p>
									Vol: 250ml<br />
									Color: White<br />
									Material: Glass
								</p>
							</div>
							<div class="item-qty">
								<label class="visible-xs">QTY</label>
								<p>25</p>
							</div>
							<!--<div class="item-date">
								<label class="visible-xs">Due Date</label>
								<p>22/03/2016</p>
							</div>-->
							<div class="item-b_add">
								<p>Dadar Bill-1</p>
								<!--<a href="#">Add another</a>-->
							</div>
						</div>
						<div class="secondBlockOuter">
							<div class="secondBlock">
								<div class="item-s_add">
									<p>Ship-1 BKC</p>
									<a href="#" data-toggle="modal" data-target="#myAdd">Add another</a>
								</div>
								<div class="item-dev_qty">
									<label class="visible-xs">QTY</label>
									<div class="quantitywrap">
										<input type='button' value='-' class='qtyminus btn' field='quantity' />
										<input type='text' name='quantity' value='1' class='qty' />
										<input type='button' value='+' class='qtyplus btn' field='quantity' />
									</div>
								</div>
								<!--<div class="item-ship_charge">
									<p><span class="rupee">₹</span> 0.00</p>
								</div>-->
								<div class="item-close">
									<a href="#"><span class="circle"><i class="glyphicon glyphicon-remove"></i></span></a>
								</div>
							</div>
							<div class="secondBlock border-top">
								<div class="item-s_add">
									<p>Ship-1 BKC</p>
									<a href="#" data-toggle="modal" data-target="#myAdd">Add another</a>
								</div>
								<div class="item-dev_qty">
									<label class="visible-xs">QTY</label>
									<div class="quantitywrap">
										<input type='button' value='-' class='qtyminus btn' field='quantity' />
										<input type='text' name='quantity' value='1' class='qty' />
										<input type='button' value='+' class='qtyplus btn' field='quantity' />
									</div>
								</div>
								<!--<div class="item-ship_charge">
									<p><span class="rupee">₹</span> 0.00</p>
								</div>-->
								<div class="item-close">
									<a href="#"><span class="circle"><i class="glyphicon glyphicon-remove"></i></span></a>
								</div>
							</div>
							 
						</div>
					</div>
					
					
					 

					<!--<div class="box-heading-footer">
						<div class="col-md-6 col-sm-6 text-left">
							<label><input type="checkbox" class="checkbox-inline" checked>I agree to <a href="#">Terms & Conditions</a></label>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<button class="btn2 orangebtn">PROCEED TO PAYMENT</button>
						</div>
					</div>-->
				</div>
				<?php include 'order-summary.php'; ?>
			</div>
		</div>
	</div>
</div>
<?php include 'footer.php'; ?>
<!-- Add Another Address -->
<div id="myAdd" class="modal fade add_address" role="dialog">
  	<div class="modal-dialog">
    	<!-- Modal content-->
    	<div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal">&times;</button>
          	</div>
      		<div class="row modal-body">
                <div class="col-md-4 col-sm-4 p20">
                    <h4 class="mb10">Select Shipping Address from saved</h4>
                    <ul>
                        <li><input type="radio" name="radio_add" class="radio-inline" value="1"> ADD 01</li>
                        <li><input type="radio" name="radio_add" class="radio-inline" value="2"> ADD 02</li>
                        <li><input type="radio" name="radio_add" class="radio-inline" value="3"> ADD 03</li>
                        <li><input type="radio" name="radio_add" class="radio-inline" value="4"> ADD 04</li>
                    </ul>
                </div>
                <div class="col-md-1 col-sm-1">
                	<h4 class="add_or">OR</h4>
                </div>
                <div class="col-md-7 col-sm-7 p20">
                    <h4 class="mb10">Add Another Address</h4>
                    <form name="frm-address" method="post" enctype="multipart/form-data" class="form-horizontal">
                    	<div class="form-group">
                        	<div class="col-md-6">
                            	<input type="text" class="form-control" placeholder="Full Name">
                            </div>
                            <div class="col-md-6">
                            	<input type="text" class="form-control" placeholder="Address Heading">
                            </div>
                        </div>
                        <div class="form-group">
                        	<div class="col-md-12">
                            	<input type="text" class="form-control" placeholder="Full Address">
                            </div>
                        </div>
                        <div class="form-group">
                        	<div class="col-md-6">
                            	<input type="text" class="form-control" placeholder="Landmark">
                            </div>
                            <div class="col-md-6">
                            	<input type="text" class="form-control" placeholder="City">
                            </div>
                        </div>
                        <div class="form-group">
                        	<div class="col-md-6">
                            	<input type="text" class="form-control" placeholder="Pincode">
                            </div>
                            <div class="col-md-6">
                            	<select class="form-control" name="cbo_state">
                                    <option value="Select">Select</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                            	<select class="form-control" name="cbo_country">
                                    <option value="Select">Select</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                            	<input type="text" class="form-control" placeholder="Phone No.">
                            </div>
                        </div>
                        <div class="form-group mt20">
                            <div class="col-md-12 col-sm-12 text-center">
                                <button type="submit" class="btn orangebtn">Save</button>
                            </div>                	
                        </div>
                    </form>
                </div>
      		</div>
    	</div>
  	</div>
</div>