		<?php include 'header.php'; ?>
		<div class="container" id="checkoutBox">
        	<div class="row">
            	<div class="col-md-12 p20 empty_cart text-center">
                	<h2><i class="glyphicon glyphicon-shopping-cart"></i> Your Shopping Cart is empty.</h2>
                    <p>Continue shopping on the <a href="#" class="bluetext">Clinito.com homepage</a></p>
                    <button class="btn orangebtn mt40">Continue Shopping</button>
                </div>
                <div class="col-md-12"><a href="#" class="pull-right mr20"><i class="glyphicon glyphicon-print"></i></a></div>
            	<div class="col-md-12">
                	<div class="row nav-box">
                        <div class="col-md-12">
                        	<ul class="checkoutNav">
                            	<li class="active">
                                	<a href="checkout-shoppingcart.php"><h4><span class="iconbox">1</span> Shopping Cart</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li>
                                	<a href="checkout-yourdetails.php"><h4><span class="iconbox">2</span> Your Details</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li>
                                	<a href="checkout-paymethod.php"><h4><span class="iconbox">3</span> Payment Methods</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li>
                                	<a href="checkout-confirmation.php"><h4><span class="iconbox">4</span> Comfirmation</h4></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                  <!--  <div class="row nav-box1">
                    	<div class="col-md-9 col-sm-12">
                        	<h4>1. Your Order</h4> 
                         	<div class="grid1">
                                <label class="form_text">Do you have form c?<input type="checkbox" class="checkbox-inline" checked></label>
                            </div>
                            <div class="grid2_1">
                            	<div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-7">
                                        <input type="text" name="txt_frm_no" placeholder="Enter Form No." class="form-control">
                                    </div>
                                    <div class="col-md-3 col-sm-3 order-part">
                                        <button class="btn3 btn-success"><i class="glyphicon glyphicon-plus"></i> Add</button>
                                    </div>
                                    <div class="col-md-3 col-sm-3 order-part text-right pr0 pt15">
                                        <button class="btn3 darkgraybtn">Print Cart</button>
                                        
                                    </div>
                               	</div>
                            </div>
                            
                            
                            
                            
                            
                        </div>-->
                    </div>
                    <div class="row nav-box1">
                    	<div class="col-md-9 col-sm-12 padding-mobile">
                        	<div class="box-heading hidden-xs">
                            	<div class="item-image">
                                	&nbsp;
                                </div>
                                <div class="item-name">
                                	<h4>Item</h4>
                                </div>
                                <div class="item-qty">
                                	<h4>Quantity</h4>
                                </div>
                                <!--<div class="item-date">
                                	<h4>Date Placed</h4>
                                </div>-->
                                <div class="item-price">
                                	<h4>Price</h4>
                                </div>
                                <div class="item-total">
                                	<h4>Total</h4>
                                </div>
                                <div class="item-action">
                                	<h4>&nbsp;</h4>
                                </div>
                            </div>
                            <div class="box-heading-inner">
                            	<div class="item-image pt15">
                                	<a href="#"><img src="images/product.jpg"></a>
                                </div>
                                <div class="item-name pt15">
                                    <h5><a href="#">Low scale beaker by kimberly</a></h5>
                                    <p>
                                        Vol: 250ml<br />
                                        Color: White<br />
                                        Material: Glass
                                    </p>
                                </div>
                                <div class="item-qty pt15">
                                	<label class="visible-xs">QTY</label>
                                    <div class="quantitywrap">
                                      	<input type='button' value='-' class='qtyminus btn' field='quantity' />
                                      	<input type='text' name='quantity' value='1' class='qty' />
                                      	<input type='button' value='+' class='qtyplus btn' field='quantity' />
                                    </div>
                                </div>
                                <!--<div class="item-date pt15">
                                	<label class="visible-xs">Due Date</label>
                                    <p>22/03/2016</p>
                                </div>-->
                                <div class="item-price pt15">
                                	<label class="visible-xs">Price</label>
                                   	<p><span class="rupee">₹</span> 500.00</p>
                                </div>
                                <div class="item-total pt15">
                                	<label class="visible-xs">Total</label>
                                  	<p><span class="rupee">₹</span> 50,000</p>
                                </div>
                                <div class="item-action pt15">
                                	<a href="#"><span class="circle graytext"><i class="glyphicon mr0 glyphicon-remove"></i></span></a>
                                </div>
                            </div>
 <div class="box-heading-inner">
                            	<div class="item-image pt15">
                                	<a href="#"><img src="images/product.jpg"></a>
                                </div>
                                <div class="item-name pt15">
                                    <h5><a href="#">Low scale beaker by kimberly</a></h5>
                                    <p>
                                        Vol: 250ml<br />
                                        Color: White<br />
                                        Material: Glass
                                    </p>
                                </div>
                                <div class="item-qty pt15">
                                	<label class="visible-xs">QTY</label>
                                    <div class="quantitywrap">
                                      	<input type='button' value='-' class='qtyminus btn' field='quantity' />
                                      	<input type='text' name='quantity' value='1' class='qty' />
                                      	<input type='button' value='+' class='qtyplus btn' field='quantity' />
                                    </div>
                                </div>
                                <!--<div class="item-date pt15">
                                	<label class="visible-xs">Due Date</label>
                                    <p>22/03/2016</p>
                                </div>-->
                                <div class="item-price pt15">
                                	<label class="visible-xs">Price</label>
                                   	<p><span class="rupee">₹</span> 500.00</p>
                                </div>
                                <div class="item-total pt15">
                                	<label class="visible-xs">Total</label>
                                  	<p><span class="rupee">₹</span> 50,000</p>
                                </div>
                                <div class="item-action pt15">
                                	<a href="#"><span class="circle graytext"><i class="glyphicon mr0 glyphicon-remove"></i></span></a>
                                </div>
                            </div>
                            <!--<div class="box-heading-footer">
                            	<div class="col-md-5 col-sm-5 order-part-footer ">
                                	<div class="form-group">
                                    	<div class="input-group">
                                          	<input type="text" class="form-control" placeholder="Enter Coupon Code">
                                          	<span class="input-group-btn">
                                            	<button class="btn btn-apply darkgraybtn" type="button">Apply</button>
                                          	</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-7 col-xs-12 text-right">
                                	<h4>TOTAL COST: <span class="rupee">₹</span> 2,00,000</h4>
                                </div>
                            </div>-->
                        </div>
                        <?php include 'order-summary.php'; ?>
                 	</div>
                    <!--<div class="row nav-box2">
                    	<div class="col-md-12 text-center">
                        	<button class="btn orangebtn">Proceed</button>
                        </div>
                    </div>-->
                </div>
           	</div>
		</div>
		<?php include 'footer.php'; ?>