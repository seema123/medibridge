<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
 
  <li class="active">Making Payments</li>
</ol>
<div class="container">
  <div class="innercontent makingpaymentwrap ">
    <div class="p20"> 
     <h1 class="titleh">Delivery options </h1>
     <div class="bgwhite ">
     	<h2>Choose from multiple delivery options </h2>
        <ul class="optin">
        	<li>
            	<h3>Single Drop</h3>
                <p>Get your entire order delivered at one designated address.</p>
            </li>
            <li>
            	<h3>Multiple Drop</h3>
                <p>For any single order you can provide multiple delivery addresses.  Specified products form the order will be dropped to designated multiple addresses as per your directions to conclude the delivery.</p>
            </li>
            
        </ul>
        
        
     </div>
    </div>
  </div>
</div>
<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>
