<!--footer start-->

<footer>
  <div class="container">
    <div class="row footer-top">
      <div class="col-sm-3 hidden-xs">
        <h4 class="footerheading ">Company</h4>
        <ul class="fLinks">
          <li><a href="#">About Us</a></li>
          <li><a href="#">Careers</a></li>
          <li><a href="faq.php">FAQs</a></li>
          <li><a href="#">Sell On Clinito</a></li>
        </ul>
      </div>
      <div class="col-sm-3 hidden-xs">
        <h4 class="footerheading">How to Buy?</h4>
        <ul class="fLinks">
          <li><a href="login.php">Create an Account</a></li>
          <li><a href="making-payment.php">Making Payments</a></li>
          <li><a href="delivery-options.php">Delivery Options</a></li>
          <li><a href="buyer-protection.php">Buyer Protection</a></li>
          <li><a href="#">New User Guide</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
      <h4 class="footerheading">Policies</h4>
        <ul class="fLinks">
          <li><a href="#">Return Policy</a></li>
          <li><a href="#">Cancellation Policy</a></li>
          <li><a href="#">Refund Policy</a></li>
          <li><a href="#">Shipping Policy</a></li>
          <li><a href="#">Terms & Conditions</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
       <!-- <h4 class="footerheading">Subscription</h4>
        <div class="subscriptionhld">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Please enter your email">
            <span class="input-group-btn">
            <button class="btn" type="button">Subscribe</button>
            </span> </div>
           
          <p class="mtb10">Register now to get updates on promotions and coupons.</p>
        </div>-->
          <h4 class="footerheading">Contact Us</h4>
        <ul class="fLinks mb10">
          <li><span class="sprite flocation"></span>201, 2nd Floor, Gagangiri, Mumbai, 400055</li>
          <li><span class="sprite fcall"></span>Toll free: 1800-300-23456 (9 AM - 8 PM)</li>
          <li><span class="sprite fmail"></span><a href="mailto:care@medibridge.com" class="blacktext">care@clinito.com</a></li>
        </ul>
        
        <ul class="social-icons">
        <h4 class="footerheading mb10">Follow Us</h4>
          <li><a href="" class="gmailicn sprite"></a></li>
          <li><a href="" class="facebbokicn sprite"></a></li>
          <li><a href="" class="twitericn sprite"></a></li>
          <li><a href="" class="instaicn sprite"></a></li>
        </ul>
      </div>
    </div>
    </div>
    <div class="footerTab"> 
        <div class="container">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#shopAllcat" aria-controls="shopAllcat" role="tab" data-toggle="tab">Shop All Categories</a></li>
        <li role="presentation"><a href="#shopbyBrand" aria-controls="shopbyBrand" role="tab" data-toggle="tab">Shop All Brands</a></li>
      </ul>
      
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="shopAllcat">
          <ul>
            <li><a href="">Anesthesia</a></li>
            <li><a href="">Apparel</a></li>
            <li><a href="">Beds and Mattresses</a></li>
            <li><a href="">Central Sterile</a></li>
            <li><a href="">Durable Medical Equipment (DME)</a></li>
            <li><a href="">Environmental Services (EVS)</a></li>
            <li><a href="">Anesthesia</a></li>
            <li><a href="">Apparel</a></li>
            <li><a href="">Beds and Mattresses</a></li>
            <li><a href="">Central Sterile</a></li>
            <li><a href="">Durable Medical Equipment (DME)</a></li>
            <li><a href="">Environmental Services (EVS)</a></li>
            <li><a href="">Anesthesia</a></li>
            <li><a href="">Apparel</a></li>
            <li><a href="">Beds and Mattresses</a></li>
            <li><a href="">Central Sterile</a></li>
            <li><a href="">Durable Medical Equipment (DME)</a></li>
            <li><a href="">Environmental Services (EVS)</a></li>
            <li><a href="">Anesthesia</a></li>
            <li><a href="">Apparel</a></li>
            <li><a href="">Beds and Mattresses</a></li>
            <li><a href="">Central Sterile</a></li>
            <li><a href="">Durable Medical Equipment (DME)</a></li>
          </ul>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="shopbyBrand">
          <ul>
            <li><a href="">Anesthesia</a></li>
            <li><a href="">Apparel</a></li>
            <li><a href="">Beds and Mattresses</a></li>
            <li><a href="">Central Sterile</a></li>
            <li><a href="">Durable Medical Equipment (DME)</a></li>
            <li><a href="">Environmental Services (EVS)</a></li>
          </ul>
        </div>
      </div>
      </div>
    </div>
  
</footer>
<a href="#" class="scrollup" style=""></a> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script> 
<!--<script src="js/bootstrap-hover-dropdown.min"></script>--> 
<script src="js/owl.carousel.min.js"></script> 
<!--<script src="http://www.menucool.com/vertical/vm/menu-vertical.js" type="text/javascript"></script>--> 
<!--filter price range--> 
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 
<!--<script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script> --> 
<script src="js/jquery.ez-plus.js"></script> 
<!--input field--> 
<script src="js/classie.js"></script> 
<!--zoom effect --> 
<!--<script src="js/jquery.elevatezoom.js"></script> --> 
<!--filter scroll--> 
<script src="js/jquery.slimscroll.min.js"></script> 
<!-- Add fancyBox main JS and CSS files --> 
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script> 
<script src="js/main.js"></script> 
<!--Mobille Menu--> 
<!--<script src="js/script.min.js" type="text/javascript"></script> -->
<!--Sticky Sidebar--> 
<!--<script src="js/test.js" type="text/javascript"></script> -->
<script src="js/theia-sticky-sidebar.js" type="text/javascript"></script> 
<script>
			$(document).ready(function() {
				$('#orderSummary')
					.theiaStickySidebar({
						additionalMarginTop: 1
				});
			});
		</script> 
<script type="text/javascript">
			$(function() {
				$("#loader-wrapper").delay(1000).fadeOut(500);
			});
		</script>
</body></html>