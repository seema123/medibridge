<?php
include 'header.php';
?>
<?php
include 'banner.php';
?>
<div class="container"> 
  <!--Top sub categories 1 and banner-->
  <div class="row catseclist">
    <div class="col-sm-3 col-md-3 p0-col catlist">
      <h2><span class="diagnosticicn sprite"></span> <span>EXAM AND <br>
        DIAGNOSTIC</span></h2>
      <ul>
        <li><a href="filter.php">AED Defibrillators</a></li>
        <li><a href="Shopping-List-main.php">Blood Pressure & Monitoring</a></li>
        <li><a href="">Diagnostic </a></li>
        <li><a href="">Testing/Monitoring</a></li>
        <li><a href="">Dopplers</a></li>
        <li><a href="">Needles & Syringes</a></li>
        <li><a href="">ECG</a></li>
        <li><a href="">Endoscopic</a></li>
        <li class="text-right"><a href="see_all.php" class="seeAll">See All</a></li>
      </ul>
    </div>
    <div class="col-sm-6 col-md-5 p0-col ">
      <div class="owl-carousel owl-theme">
        <div class="item"> <img src="images/banner/banner1_img1.jpg" class="img-responsive"  >
          <div class="productwrapper">
            <div class="product-name"><a href="product_details.php">Experimentation and
              Research</a></div>
            <div class="pricehld">
              <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
              <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
            </div>
          </div>
        </div>
        <div class="item"> <img src="images/banner/banner1_img1.jpg" class="img-responsive"  >
          <div class="productwrapper">
            <div class="product-name"><a href="product_details.php">Experimentation and
              Research</a></div>
            <div class="pricehld">
              <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
              <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3 col-md-4 p0-col ">
      <div class="rec-industry-list">
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
      </div>
    </div>
  </div>
  
  <!--Top sub categories 1 and banner-->
  <div class="row catseclist">
    <div class="col-sm-3 col-md-3 p0-col catlist">
      <h2><span class="environmenticn sprite"></span> <span>Environmental<br>

Services (EVS)</span></h2>
      <ul>
        <li><a href="filter.php">AED Defibrillators</a></li>
        <li><a href="Shopping-List-main.php">Blood Pressure & Monitoring</a></li>
        <li><a href="">Diagnostic </a></li>
        <li><a href="">Testing/Monitoring</a></li>
        <li><a href="">Dopplers</a></li>
        <li><a href="">Needles & Syringes</a></li>
        <li><a href="">ECG</a></li>
        <li><a href="">Endoscopic</a></li>
        <li class="text-right"><a href="" class="seeAll">See All</a></li>
      </ul>
    </div>
    <div class="col-sm-6 col-md-5 p0-col ">
      <div class="owl-carousel owl-theme">
        <div class="item"> <img src="images/banner/banner1_img1.jpg" class="img-responsive"  >
          <div class="productwrapper">
            <div class="product-name"><a href="product_details.php">Experimentation and
              Research</a></div>
            <div class="pricehld">
              <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
              <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
            </div>
          </div>
        </div>
        <div class="item"> <img src="images/banner/banner1_img1.jpg" class="img-responsive"  >
          <div class="productwrapper">
            <div class="product-name"><a href="product_details.php">Experimentation and
              Research</a></div>
            <div class="pricehld">
              <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
              <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3 col-md-4 p0-col ">
      <div class="rec-industry-list">
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
      </div>
    </div>
  </div>
  
  <!--Top sub categories 1 and banner-->
  <div class="row catseclist">
    <div class="col-sm-3 col-md-3 p0-col catlist">
      <h2><span class="labicn sprite"></span> <span>Lab Supplies</span></h2>
      <ul>
        <li><a href="filter.php">AED Defibrillators</a></li>
        <li><a href="Shopping-List-main.php">Blood Pressure & Monitoring</a></li>
        <li><a href="">Diagnostic </a></li>
        <li><a href="">Testing/Monitoring</a></li>
        <li><a href="">Dopplers</a></li>
        <li><a href="">Needles & Syringes</a></li>
        <li><a href="">ECG</a></li>
        <li><a href="">Endoscopic</a></li>
        <li class="text-right"><a href="" class="seeAll">See All</a></li>
      </ul>
    </div>
    <div class="col-sm-6 col-md-5  p0-col ">
      <div class="owl-carousel owl-theme">
        <div class="item"> <img src="images/banner/banner1_img1.jpg" class="img-responsive"  >
          <div class="productwrapper">
            <div class="product-name"><a href="product_details.php">Experimentation and
              Research</a></div>
            <div class="pricehld">
              <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
              <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
            </div>
          </div>
        </div>
        <div class="item"> <img src="images/banner/banner1_img1.jpg" class="img-responsive"  >
          <div class="productwrapper">
            <div class="product-name"><a href="product_details.php">Experimentation and
              Research</a></div>
            <div class="pricehld">
              <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
              <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3 col-md-4 p0-col ">
      <div class="rec-industry-list">
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href="product_details.php"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="product_details.php">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
        <div class="productwrapper"> <a href=""><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
          <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
        </div>
      </div>
    </div>
  </div>
  
  <!--ProductSlider start-->
  <div class="row ProductSlider">
    <h2>Featured Products</h2>
    <div class="owl-carousel">
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product2.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--featureSlider end--> 
</div>

<!--sell on medibridge-->
<div class="sellOnMedibridge">
  <div class="container">
    <h2>SELL ON CLINITO</h2>
    <ul>
      <li>Register With Us</li>
      <li><img src="images/arrow.png" width="12" height="12"></li>
      <li>Upload Your Products</li>
      <li><img src="images/arrow.png" width="12" height="12"></li>
      <li>Start Selling</li>
    </ul>
    <a href="" class="btn orangebtn">Start Selling Now</a> </div>
</div>
<div class="container">
  <div class="row ProductSlider">
    <h2>New Launch</h2>
    <div class="owl-carousel">
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="productwrapper"> <a href="" class="product-img"><img src="images/product.jpg" class="img-responsive" ></a>
          <div class="product-name"><a href="">Experimentation and
            Research</a></div>
          <div class="pricehld">
            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
            <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row BrandSlider">
    <h2>Featured Brands</h2>
    <div class="owl-carousel">
      <div class="item">
      <img src="images/brand/kimble.jpg" >
      <img src="images/brand/john.jpg" >
      <img src="images/brand/ng.jpg" > 
      </div>
      <div class="item"><img src="images/brand/john.jpg" ><img src="images/brand/kimble.jpg" ><img src="images/brand/john.jpg" > </div>
      <div class="item"><img src="images/brand/ng.jpg" ><img src="images/brand/ng.jpg" ><img src="images/brand/ng.jpg" > </div>
      <div class="item"><img src="images/brand/kimble.jpg" ><img src="images/brand/john.jpg" ><img src="images/brand/john.jpg" > </div>
      <div class="item"><img src="images/brand/kimble.jpg" ><img src="images/brand/kimble.jpg" ><img src="images/brand/kimble.jpg" > </div>
      <div class="item"><img src="images/brand/john.jpg" ><img src="images/brand/ng.jpg" ><img src="images/brand/kimble.jpg" > </div>
    </div>
  </div>
</div>
<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>