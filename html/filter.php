<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
  <li><a href="#">Lab Supplies </a></li>
  <li class="active">Beakers</li>
</ol>
<div class="container">
  <div class="filtersection row">
    <div class="col-md-3 p0 hidden-sm hidden-xs">
      <div class="filter-sidebar">
        <!--<div class="selectedlistbtn"> <a href=" " class="btn bluebtn">NURSING HOME</a></div>-->
        
        <div class="filter-options">
        
        <div class="filter-options">
          <h4 class="tname">CATEGORIES<span class="sprite"></span></h4>
          <div class="filteroption">
            <div class="categoriesfilter">
                <ul class="vscroll">
                  <li >
                    <a href="javascript:void(0)" class="subdropdown headingcat "  >Lab Supplies</a>
                    <ul>
                      <li class="active">
                      <a href="" class="">Beakers</a>
                        <!--<h3 class="subdropdown headingcat "> Beakers</h3>
                        <ul>
                          <li class="active"><a href="" >Beakers</a></li>
                          <li class=""><a href="" >Blood Collection</a></li>
                          <li class=""><a href="" >Caseettes</a></li>
                        </ul>-->
                      </li>
                      <li class=""><a href="" >Blood Collection</a></li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                  <li class="closed" >
                    <a href="javascript:void(0)" class="subdropdown headingcat "  >Anesthesia</a>
                    <ul>
                      <li class="closed"><a href="javascript:void(0)" class="subdropdown headingcat " >Blood Collection</a>
                    
                        <ul>
                          <li class="active"><a href="" >Beakers</a></li>
                          <li class=""><a href="" >Blood Collection</a></li>
                          <li class=""><a href="" >Caseettes</a></li>
                        </ul>
                      </li>
                      <li class=""><a href="" >Caseettes</a></li>
                    </ul>
                  </li>
                </ul>
              
            
          </div>
            
          </div>
        </div>
        
          
        </div>
        <div class="filter-options">
          <h4 class="tname">Price Range<span class="sprite"></span></h4>
          <div class="filteroption price-slider">
            <ul class="rangeinputslider text-center">
              <li><span class="rupee">₹</span>
                <input type="text" class="sliderValue" data-index="0" value="10" />
              </li>
              <li>&#8212;</li>
              <li><span class="rupee">₹</span>
                <input type="text" class="sliderValue " data-index="1" value="90" />
              </li>
            </ul>
            <br />
            <div id="slider-range"></div>
          </div>
        </div>
        <div class="filter-options">
          <h4 class="tname">TAGS<span class="sprite"></span></h4>
          <div class="filteroption">
            <ul class="vscroll">
              <li class="checkbox">
                <label> 
                  <input name="" type="checkbox" value="" disabled >
                  Consumable </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="" disabled checked>
                  Supply </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  Equipment </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  Device </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  Instrument </label>
              </li>
            </ul>
          </div>
        </div>
        <div class="filter-options">
          <h4 class="tname">BRAND<span class="sprite"></span></h4>
          <div class="filteroption">
            <ul class="vscroll">
              <li class="checkbox">
                <label> 
                  <input name="" type="checkbox" value="" disabled >
                  3XL </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="" disabled checked>
                  3XL </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  3XL </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  3XL </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  3XL </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  3XL </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  3XL </label>
              </li>
              <li class="checkbox">
                <label>
                  <input name="" type="checkbox" value="">
                  3XL </label>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!--filter-sidebar end--> 
    </div>
    <div class="col-sm-12   col-md-9 filter-rgt">
      <div class=" ">
      <div class="row">
      <div class="filter-selected hidden-sm">
          <!--<span class="selClose sprite"></span>-->
          <ul class="">
            <li>
            <div class="filtertext"><b>568 results for Beakers :</b> </div>
            <div class="filtertextsel">
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
                <span class="filselected">SISCO<i class="glyphicon glyphicon-remove"></i></span>
          		<a class="clearAll" href="">Clear All</a>
          </div>
  </li>
          </ul>
          </div>
        <div class="filterview  ">
          <!--<div class="comparesec"> <span>Compare: </span>
            <ul>
              <li><img src="images/product_300.jpg" >
                <div class="imghover">x</div>
              </li>
              <li><img src="images/product_300.jpg" >
                <div class="imghover">x</div>
              </li>
              <li><img src="images/product_300.jpg" >
                <div class="imghover">x</div>
              </li>
            </ul>
            <button type="button" class="btn orangebtn">Compare Now</button>
            <span class="prompt">Max 3 Product will add</span> </div>-->
          <button type="button" class="btn visible-sm  visible-xs orangebtn filter-ipad-btn">FILTER</button>
          <div class="viewsec"> <span>View | </span>
            <div class="btn-group"> <a href="#" id="list" class="btn-sm"><span class="glyphicon glyphicon-th-list"> </span></a> <a href="#" id="grid" class="btn-sm active"><span
                class="glyphicon glyphicon-th"></span></a> </div>
          </div>
        </div>
         
        <!--<div class="searchresultsecfilter">
          <h4>My Nursing Home List <span>(50)</span></h4>
          <h5 class="hidden-xs">Tips</h5>
        </div>-->
       </div>
        <div id="products" class="row list-group productwrap">
           
        	<div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="product_details.php" class="product-item-photo"><img src="images/product/BD Vacutainer SST Tubes(367977).jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <a  class="btn-viewDetail btn" href="product_details.php" ><span>View Details</span></a>
                  <a  href="" class="btn-addToCart btn"><span>Add to Cart</span></a>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="product_details.php">Heavy Low Scale Beakers
by Kimble Chase</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
    		 <div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="" class="product-item-photo"><img src="images/product/DISPOVAN SINGLE USE NEEDLES green.jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <button type="button" class="btn-viewDetail btn"><span>View Details</span></button>
                  <button type="button" class="btn-addToCart btn"><span>Add to Cart</span></button>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="">Experimentation and Research</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="" class="product-item-photo"><img src="images/product/PORTABLE STEAM STERICLAVE (NON-E) ST1214MN.jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <button type="button" class="btn-viewDetail btn"><span>View Details</span></button>
                  <button type="button" class="btn-addToCart btn"><span>Add to Cart</span></button>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="">Experimentation and Research</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="" class="product-item-photo"><img src="images/product_300.jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <button type="button" class="btn-viewDetail btn"><span>View Details</span></button>
                  <button type="button" class="btn-addToCart btn"><span>Add to Cart</span></button>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="">Experimentation and Research</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            	<div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="" class="product-item-photo"><img src="images/product_300.jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <button type="button" class="btn-viewDetail btn"><span>View Details</span></button>
                  <button type="button" class="btn-addToCart btn"><span>Add to Cart</span></button>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="">Experimentation and Research</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    		 <div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="" class="product-item-photo"><img src="images/product2.jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <button type="button" class="btn-viewDetail btn"><span>View Details</span></button>
                  <button type="button" class="btn-addToCart btn"><span>Add to Cart</span></button>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="">Experimentation and Research</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="" class="product-item-photo"><img src="images/product3.jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <button type="button" class="btn-viewDetail btn"><span>View Details</span></button>
                  <button type="button" class="btn-addToCart btn"><span>Add to Cart</span></button>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="">Experimentation and Research</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item  col-xs-6 col-xss-12 col-sm-4 col-sm-3  ">
            <div class="productwrapper">
              <div class="thumbnail-img"><a href="" class="product-item-photo"><img src="images/product_300.jpg" class="img-responsive" ></a>
                <div class="box-hover">
                  <button type="button" class="btn-viewDetail btn"><span>View Details</span></button>
                  <button type="button" class="btn-addToCart btn"><span>Add to Cart</span></button>
                </div>
              </div>
              <div class="product-info">
                <div class="product-name"><a href="">Experimentation and Research</a></div>
                <div class="pricehld">
                  <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                  <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                </div>
                <div class="shoppinlistcomparef">
                  <div class="ml">200 ML</div>
                  <div class="compare ">
                    <div class="comparecheck checkbox">
                      <label class="">
                        <input name="" type="checkbox" value="">
                        Compare </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
      
      <!--Pagination-->
      <div class="text-right">
        <ul class="pagination">
          <li><a href="#">Prev</a></li>
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">Next</a></li>
        </ul>
      </div></div>
      
      <h4 class="productResultCount"><span>0 </span>Products Found</h4>
    </div>
  </div>
  
  <!--Tips for buyer-->
  
  <div class="row">
    <div class="col-sm-6 ">
      <div class="buyerTips">
        <h4>Tips for Buying</h4>
        <ul class="list-orangeArrow">
          <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
          <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
        </ul>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="buyerprotection">
        <h4>Buyer Protection</h4>
        <ul class="list-orangeRoundArrow">
          <li>Full Refund if you don't receive your order</li>
          <li>Refund or Keep items not as described</li>
        </ul>
      </div>
    </div>
  </div>
  
  <!--container closed--> 
</div>

<!--sell on medibridge-->

<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>