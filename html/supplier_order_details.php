<?php
	include 'header.php';
?>
<!--bredcrumbs-->
<ol class="breadcrumb container">
  	<li><a href="#">Home</a></li>
  	<li><a href="#">Orders </a></li>
  	<li class="active">ORD00ABF00001</li>
</ol>
<div class="container orderDetailssec">
    <!--Order details-->
    <div class="row top-cancel">
    	<div class="col-md-12 col-sm-12">
        	<table width="100%">
            	<tr>
                	<td width="50%" class="text-left">
                    	<h2>Partial Cancelled</h2>
                   	</td>
                    <td width="50%" class="text-right">
                    	17<sup>th</sup> March 2016
                   	</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row head-ord p20 mb20">
        <div class="col-md-12 col-sm-12">
            <h1>Order Details</h1>
            <ul>
                <li>Ordered on 17<sup>th</sup> March 2016</li>
                <li>|</li>
                <li><span class="redtext">Due Date For Pick Up 17<sup>th</sup> March 2016</span></li>
                <li>|</li>
                <li>Order #4000041234-1234</li>
                <li>|</li>
                <li>3 Items</li>
                <li>|</li>
                <li>Multiple Drops</li>
            </ul>
        </div>
        <div class="col-md-12 col-sm-12 orderAdd">
            <div class="col-sm-5">
                <h4>Billing Address</h4>
                <p>Nishi Kant</p>
                <p>98765 43210</p>
                <p>Shree Shaswat, Blnd no. 02, flat no. 10,</p>
                <p>Pleasant Park, Carter road, Bandra west.</p>
                <p>Mumbai, Maharahstra - 400 050.</p>
            </div>
            <div class="col-sm-4">
            	 <h4>Payment Mode</h4>
                 <p>Visa</p>
            </div>
            <div class="col-sm-3">
                <h4>Order Summary</h4>
                <p>Item(s) Subtotal : Rs. 8550.00</p>
                <p class="redtext">Cancelled Total : Rs. -4000.00</p>
                <p>Clinito fees : Rs. 950.00</p>
                <p>Total : Rs. 9550.00</p>
                <p class="mt10"><strong>Grand Total : Rs. 9550.00</strong></p>
            </div>
        </div>
    </div>
    <div class="row body-ord">
    	<div class="col-md-12 col-sm-12 box-heading">
        	<h3>Product Details</h3>
        </div>
 	</div>
    <div class="row body-ord">
        <div class="prd-details  table-responsive">
        	<table width="100%" class="table">
            	<tr>
                	<td width="10%">
                    	<a href="#">
                        	<img src="images/product4.jpg" width="100">
                        </a>
                    </td>
                    <td width="20%" valign="middle" class="rbrd">
                    	<a href="#" class="bluetext text-left">
                        	<strong>Solstice Nitril Powder <br />- Free Exam Gloves</strong>
                        </a>
                    </td>
                    <td valign="middle" class="text-center data-ord  rbrd">
                    	<span><strong>SKU Number</strong></span><br />
                        <span><strong>79867543677</strong></span>
                    </td>
                    <td valign="middle" class="text-center data-ord rbrd">
                    	<span><strong>Cipla</strong></span>
                    </td>
                    <td valign="middle" class="text-center data-ord rbrd">
                    	<span><strong>Blue</strong></span>
                    </td>
                    <td valign="middle" class="text-center data-ord rbrd">
                    	<span><strong>Latex</strong></span>
                    </td>
                    <td valign="middle" class="text-center data-ord rbrd">
                    	<span><strong>Rs. 4000</strong></span>
                    </td>
                    <td width="300" valign="middle" class="text-center data-ord rbrd">
                   	  	<select class="form-control mb10 mr20 outstocksel">
            	<option value="Out Of Stock">Out Of Stock</option>
                <option value="Ready for Shipping">Available</option>
            </select>
            <input type="submit" value="Save" class="btn orangebtn">
                    </td>
                  
                </tr>
            </table>
        </div>
    </div>
    <div class="row content-ord  table-responsive">
<table width="100%" class="table">
    <tr>
      <td width="10%" valign="middle" class="data-ord text-center rbrd">Parcel 1</td>
        <td width="10%" valign="middle" class="data-ord text-center">
            <h3>3 Box</h3>
        </td>
        <td width="25%" valign="middle" class=" data-ord">
          <p>Nishi Kant, 98765 43210</p>
<p>Shree Shaswat, Blnd no. 02, flat no. 10,</p>
<p>Pleasant Park, Carter road, Bandra west.</p>
<p>Mumbai, Maharahstra - 400 050.</p>
        </td>
        <td valign="middle" class="data-ord text-center">
          <span><strong>Tracking Id:</strong><br />
          5476876874686</span>
        </td>
        <td width="15%" valign="middle" class="data-ord text-center">
          <span><strong>Delivered On</strong><br />17<sup>th</sup> March 2016</span>
        </td>
            
        <td width="140" valign="middle" class="text-center data-ord">
             <a href="#supplyermdl"   class="bluetext" data-toggle="modal" data-target="#supplyermdl">+ Add Packaging</a> 
        </td>
          <td width="" valign="middle" class="data-ord text-center">
       <input type="submit" value="Ready for Shipping" class="btn lightgraybtn">
        </td>
    </tr>
</table>
    </div>
      <div class="row content-ord  table-responsive">
    <table width="100%" class="table">
    <tr>
      <td width="10%" valign="middle" class="data-ord text-center rbrd">Parcel 1</td>
        <td width="10%" valign="middle" class="data-ord text-center">
            <h3>3 Box</h3>
        </td>
        <td width="25%" valign="middle" class=" data-ord">
          <p>Nishi Kant, 98765 43210</p>
<p>Shree Shaswat, Blnd no. 02, flat no. 10,</p>
<p>Pleasant Park, Carter road, Bandra west.</p>
<p>Mumbai, Maharahstra - 400 050.</p>
        </td>
        <td valign="middle" class="data-ord text-center">
          <span><strong>Tracking Id:</strong><br />
          5476876874686</span>
        </td>
        <td width="15%" valign="middle" class="data-ord text-center">
          <span><strong>Delivered On</strong><br />17<sup>th</sup> March 2016</span>
        </td>
            
        <td width="140" valign="middle" class="text-center data-ord">
             <a href="#supplyermdl"   class="bluetext" data-toggle="modal" data-target="#supplyermdl">+ Add Packaging</a> 
        </td>
          <td width="" valign="middle" class="data-ord text-center">
       <input type="submit" value="Ready for Shipping" class="btn lightgraybtn">
        </td>
    </tr>
</table>
    </div>
     
 
<!-- Modal -->
<div class="modal fade" id="supplyermdl" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Package details</h4>
      </div>
      <div class="modal-body">
        	<div class="row">
                <div class="col-sm-6"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">Number of Package</label>
                    <input type="number" class="form-control numberpackg"   name="" value="1" min="" max="">
                    </div></div>
                    <div class="col-sm-6"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">Package Type</label>
                     <select class="form-control">
                     	<option selected>Same</option>
                        <option>Different</option>
                     </select>
                    </div></div>
                </div>
                
             <div class="parcelwrap">
             <h4 class="text-center">Parcel 1</h4>
             	<div class="row">
                <div class="col-sm-6"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">Parcel Weight (In kg)</label>
                    <input type="text" class="form-control"  placeholder="Enter weight">
                    </div></div>
                    <div class="col-sm-6"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">invoice Value of the parcel</label>
                      <input type="text" class="form-control"  placeholder="Enter In Rs">
                    </div></div>
                </div>
                <div class="row">
                <div class="col-sm-12"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">Dimension(in cm)</label>
                    
                   <div class="row">
                   <div class="col-sm-4"> 
                    <div class="form-group">
                      <input type="text" class="form-control"  placeholder="Enter length">
                    </div>
                    </div>
                   
                   <div class="col-sm-4"> 
                    <div class="form-group">
                      <input type="text" class="form-control"  placeholder="Enter width">
                    </div>
                    </div>
                    
                    <div class="col-sm-4"> 
                    <div class="form-group">
                      <input type="text" class="form-control"  placeholder="Enter height">
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                     
                    
                </div>
             </div>
             
             <div class="parcelwrap">
             <h4 class="text-center">Parcel 2</h4>
             	<div class="row">
                <div class="col-sm-6"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">Parcel Weight (In kg)</label>
                    <input type="text" class="form-control"  placeholder="Enter weight">
                    </div></div>
                    <div class="col-sm-6"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">invoice Value of the parcel</label>
                      <input type="text" class="form-control"  placeholder="Enter In Rs">
                    </div></div>
                </div>
                <div class="row">
                <div class="col-sm-12"> 
                    <div class="form-group">
                    <label for="exampleInputEmail1">Dimension(in cm)</label>
                    
                   <div class="row">
                   <div class="col-sm-4"> 
                    <div class="form-group">
                      <input type="text" class="form-control"  placeholder="Enter length">
                    </div>
                    </div>
                   
                   <div class="col-sm-4"> 
                    <div class="form-group">
                      <input type="text" class="form-control"  placeholder="Enter width">
                    </div>
                    </div>
                    
                    <div class="col-sm-4"> 
                    <div class="form-group">
                      <input type="text" class="form-control"  placeholder="Enter height">
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                     
                    
                </div>
             </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn orangebtn">Save changes</button>
      </div>
    </div>
  </div>
</div>
  
</div>
<!--sell on medibridge-->
<?php
	include 'footer-top.php';
?>
<?php
	include 'footer.php';
?>
