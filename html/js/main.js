// main Search bar on home page top
// Owl carousel Category on home page
// Owl carousel ProductSlider on home page
// Owl carousel homepageslider on home page
// Owl carousel BrandSlider on home page
// Scroll to top page right button
// top sign in  popup hover effect
// Slimscroll for filter left
// filter click 
// Filter  change div animation



//main Search bar on home page top
$('#searchAllCategories li').on('click', function() {
	var title = $(this).html();
	if(title.length < 15 ) {
		$('#dropdownSearchText').html(title);
	}
else{
		var shortText = jQuery.trim(title).substring(0, 15).split(" ").slice(0, -1).join(" ") + "..";
		$('#dropdownSearchText').html(shortText);
		
	}
});

// Owl carousel homepageslider on home page
$('.homepageslider .owl-carousel').owlCarousel({
	  margin:0,
      slideSpeed : 300,
	    navigation : false, // Show next and prev buttons
	   //Pagination
		pagination : true,
        paginationSpeed : 400,
      singleItem:true
    
});

// Owl carousel Category on home page
$('.catseclist .owl-carousel').owlCarousel({
	margin:0,
	navigation : false, // Show next and prev buttons
	//Pagination
	margin:0,
	pagination : true,
	paginationSpeed : 400,
	singleItem: true,
	
});

// Owl carousel ProductSlider on home page
var owlProductslider = $(".ProductSlider .owl-carousel");
owlProductslider.owlCarousel({
    	 navigation : true, // Show next and prev buttons
         pagination : false,
      itemsCustom : [
        [0, 1],
        [450, 2],
        [650, 3],
        [800, 4],
		[1100, 5],
 
      ],
  });
 
// Owl carousel BrandSlider on home page
$('.BrandSlider .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
	 navigation : true, // Show next and prev buttons
     pagination : false,
	 itemsCustom : [
        [0, 2],
        [450, 2],
        [600, 2],
        [700, 4],
        [1000, 4],
		[1100, 5],
      ],
});

// Owl carousel See All on home page
$('.SeeAll_Slider .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
	 navigation : true, // Show next and prev buttons
     pagination : false,
	 itemsCustom : [
        [0, 2],
        [450, 2],
        [600, 2],
        [700, 4],
        [1000, 4],
		[1100, 5],
      ],
});

// Scroll to top page right button
$(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
	
// top sign in  popup hover effect 
$('.top-nav-links   li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

// Slimscroll for filter left
var filterheight = $('.filter-sidebar .filteroption .vscroll, .filter-selected ul:first').each(function() {
 
if($(this).height() >= 150){
$(this).slimScroll({
    height: '150px',
	alwaysVisible: true
});
}
});

//Top category slim scroll
$('.vscroll_category').slimScroll({
    color: '#ddd',
    alwaysVisible: true
});

//faq
$('.faqwrap .tname').on('click', function(){
	if($(this).parent('.filter-options').hasClass('closed')){
			$(this).parents('.faqhld').find('.filter-options').each(function( index ) {
				$(this).addClass('closed');
				$(this).find('.tname').addClass('closed');
				$(this).find('.filteroption').slideUp();
			});
		$(this).parent('.filter-options').find('.filteroption').slideDown();
		$(this).parent('.filter-options').removeClass('closed') ;
		$(this).removeClass('closed'); 
	}else {
		$(this).parent('.filter-options').addClass('closed') ; 
		$(this).addClass('closed');
		$(this).parent('.filter-options').find('.filteroption').slideUp();
	}
});

// filter left categories heading options
$('.faqwrap .categoriesfilter .subdropdown').on('click', function(){
	if($(this).parent('li').hasClass('closed')){
		
		$(this).parents('.categoriesfilter').find('.sublink').each(function( index ) {
			console.log($(this).attr('class'));
			$(this).addClass('closed');
			$(this).find('ul').slideUp();
		});
		
			$(this).next('ul:first').slideDown('fast');
			$(this).parent('li').removeClass('closed');
			
	}else {
		console.log(1);
		$(this).parent('li').find('.subdropdown').each(function( index ) {
			$(this).next('ul:first').slideUp('fast');
  			$(this).parent('li').addClass('closed');
		});
	}
});


//filter left heading filter option click like sizes, brand
$('.filter-sidebar .tname').on('click', function(){
	$(this).toggleClass('closed').siblings('.filteroption').slideToggle(300);
});

// filter left categories heading options
$('.filter-sidebar .categoriesfilter .subdropdown').on('click', function(){
	if($(this).parent('li').hasClass('closed')){
			$(this).next('ul:first').slideDown('fast');
			$(this).parent('li').removeClass('closed');
	}else {
		$(this).parent('li').find('.subdropdown').each(function( index ) {
			$(this).next('ul:first').slideUp('fast');
  			$(this).parent('li').addClass('closed');
		});
	}
});

// filter shopping list page compare add to list button
$('.addtoListbtn').on('click', function(){ 
	$(this).parent('.compare').find('.addtolistpopup').slideUp().show();
});

//filter shopping list page compare add to list Popup
$('.addtolistpopup .close').on('click', function(){ 
	$(this).parent('.addtolistpopup').slideDown().hide();
});

// Filter  change div animation grid to list grid
$('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
$('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});

// filter list grid and grid add active class
$('.viewsec a').on('click', function(){
	$(this).parents('.viewsec').find('.active').removeClass('active');
	$(this).addClass('active');		
});

	
// Filter close selected option of filter
$('.selClose').on('click', function(){

});
//if( ){
//	$(this).fadeOut(500, function () {
//		$(this).parent('li').remove();
//    });
//}


// Filter left range slider  start
$("#slider-range").slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
	slide: function(event, ui) {
		for (var i = 0; i < ui.values.length; ++i) {
			$("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
			 
		}
	}
});

$("input.sliderValue").change(function() {
	var $this = $(this);
	$("#slider").slider("values", $this.data("index"), $this.val());
});
// Filter left range slider end

	 
// input field js classec.js call
(function() {
	// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
	if (!String.prototype.trim) {
		(function() {
			// Make sure we trim BOM and NBSP
			var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			String.prototype.trim = function() {
				return this.replace(rtrim, '');
			};
		})();
	}

	[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
		// in case the input is already filled..
		if( inputEl.value.trim() !== '' ) {
			classie.add( inputEl.parentNode, 'input--filled' );
		}

		// events:
		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	} );

	function onInputFocus( ev ) {
		classie.add( ev.target.parentNode, 'input--filled' );
	}

	function onInputBlur( ev ) {
		if( ev.target.value.trim() === '' ) {
			classie.remove( ev.target.parentNode, 'input--filled' );
		}
	}
})();
		
	
// Login / Register nav tab button
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	$(".loginf").show()
	$(".retrieve-password").hide()
    var target = this.href.split('#');
    $('.loginLeftPanel .nav a').filter('a[href="#'+target[1]+'"]').tab('show');
})


$(".forgot-password").click(function(){
	$(".loginf").hide()
    $(".retrieve-password").fadeIn()
});

//quantiy varent plus minus counter
    // This button will increment the value
$('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($(this).parent('.quantitywrap').find('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $(this).parent('.quantitywrap').find('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $(this).parent('.quantitywrap').find('input[name='+fieldName+']').val();
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($(this).parent('.quantitywrap').find('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        //if (!isNaN(currentVal) && currentVal > 0) {
			if (!isNaN(currentVal) && currentVal > 1) {
            // Decrement one
            $(this).parent('.quantitywrap').find('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $(this).parent('.quantitywrap').find('input[name='+fieldName+']').val();
        }
    });
	

 // filter left categories heading options
$('.vertical-navbar .subdropdown').on('click', function(){
	if($(this).parent('li').hasClass('closed')){
		$(this).next('ul:first').slideDown('fast');
		$(this).parent('li').removeClass('closed');
	}else {
		$(this).next('ul:first').slideUp('fast');
		$(this).parent('li').addClass('closed');
	}
});

// Sticky header
$(window).on("resize", function () {
var windowwidth = $(window).width();
if ($(window).width() >= 768) {
	 
$('#topNavmain').affix({
    offset: {
        top: $('.toplinks').height()
      }
});
$('#sticky').affix({
    offset: {
        top: $('#sticky').height()
      }
});
}
}).resize(); 	

//Product details slider start
//Window.resize Event
$(window).on("resize", function () {
	 zoomprodctdetail();
}).resize();

zoomprodctdetail();

function zoomprodctdetail() {
	var hgtpop = $(window).height();
	var windowwidth = $(window).width();
	
	// if (windowwidth >= 768) {
				/*$("#zoom_03").ezPlus({
                    gallery: 'gallery_01',
                    cursor: 'pointer',
                    galleryActiveClass: "active",
                    imageCrossfade: true,
					 zoomType: 'inner',
					 responsive:'true',
					 easing:'true',
					 borderColour: '#ccc',
        			borderSize: 1,
					 
                //    loadingIcon: "images/spinner.gif"
                });*/

               /* $("#zoom_03").bind("click", function (e) {
                    var ez = $('#zoom_03').data('ezPlus');
                    ez.closeAll();  
                   $.fancybox(ez.getGalleryList());
                    return false;
                });*/
	// }
 //	else {}
 
 
 
 var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:false,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 4,
    itemsDesktop      : [1199,10],
    itemsDesktopSmall     : [979,10],
    itemsTablet       : [768,8],
    itemsMobile       : [479,4],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
 
 
 	}
//Product details slider end