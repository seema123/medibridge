<?php
include 'header.php';
?>
<!--bredcrumbs-->
<ol class="breadcrumb container">
  	<li><a href="#">Home</a></li>
  	<li class="active">Offers</li>
</ol>
<div class="container">
  	<div class="offersection row">
    	<div class="col-md-12 p0 hidden-xs">
    		<!--filter-sidebar end-->
            <div class="box-heading">
            	<h2 class="whiteText">Current Offers</h2>
            </div>
            <div class="row box-content">
            	<div class="col-md-4 col-sm-6">
                	<div class="box-data">
                		<p>Use Coupon Code</p>
                    	<h2 class="orangetext">Clini10</h2>
                        <hr>
                        <h4>Flat Rs 1000 off on orders above Rs 25000, Limited period offer</h4>
                        <p><a href="#" data-toggle="modal" data-target="#myModal">T&C Apply</a></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                	<div class="box-data">
                		<p>Use Coupon Code</p>
                    	<h2 class="orangetext">Clini10</h2>
                        <hr>
                        <h4>Flat Rs 1000 off on orders above Rs 25000, Limited period offer</h4>
                        <p><a href="#" data-toggle="modal" data-target="#myModal">T&C Apply</a></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                	<div class="box-data">
                		<p>Use Coupon Code</p>
                    	<h2 class="orangetext">Clini10</h2>
                        <hr>
                        <h4>Flat Rs 1000 off on orders above Rs 25000, Limited period offer</h4>
                        <p><a href="#" data-toggle="modal" data-target="#myModal">T&C Apply</a></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                	<div class="box-data">
                		<p>Use Coupon Code</p>
                    	<h2 class="orangetext">Clini10</h2>
                        <hr>
                        <h4>Flat Rs 1000 off on orders above Rs 25000, Limited period offer</h4>
                        <p><a href="#" data-toggle="modal" data-target="#myModal">T&C Apply</a></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                	<div class="box-data">
                		<p>Use Coupon Code</p>
                    	<h2 class="orangetext">Clini10</h2>
                        <hr>
                        <h4>Flat Rs 1000 off on orders above Rs 25000, Limited period offer</h4>
                        <p><a href="#" data-toggle="modal" data-target="#myModal">T&C Apply</a></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                	<div class="box-data">
                		<p>Use Coupon Code</p>
                    	<h2 class="orangetext">Clini10</h2>
                        <hr>
                        <h4>Flat Rs 1000 off on orders above Rs 25000, Limited period offer</h4>
                        <p><a href="#" data-toggle="modal" data-target="#myModal">T&C Apply</a></p>
                    </div>
                </div>
            </div>
    	</div>
  	</div>
  	<!--container closed--> 
</div>
<!--sell on medibridge-->
<!-- Modal -->
<div id="myModal" class="modal fade offerSec" role="dialog">
  	<div class="modal-dialog">
    	<!-- Modal content-->
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">Close &times;</button>
        		<h4 class="modal-title orangetext">Clini10</h4>
      		</div>
      		<div class="modal-body">
        		<h4>Terms & Conditions</h4>
                <ol type="1">
                	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </li>
                    <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                    <li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </li>
                    <li>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                    <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</li>
                    <li>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit </li>
                    <li>aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </li>
                    <li>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </li>
                    <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis</li>
                    <li>autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur.</li>
                </ol>
      		</div>
    	</div>
  	</div>
</div>
<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>