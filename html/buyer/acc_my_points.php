<div class="box">
	<div class="row header">
    	<div class="col-md-6 col-sm-6">
        	<h4>My Points</h4>
      	</div>
        <div class="col-md-6 col-sm-6 text-right">
        	<a href="#">Points FAQs</a> | <a href="#">Terms of use</a> 
      	</div>
 	</div>
    <div class="tab-content">
        <div id="pwd" class="tab-pane points fade in active">
            <table class="table table-condensed">
            	<thead>
                	<tr>
                    	<th colspan="2" align="left" width="50%"><h4>MediBridge Points</h4></th>
                        <th colspan="2" width="50%" style="text-align:right;"><h4><strong><span class="orangetext">8,000</span> <small>Points</small></strong></h4></th>
                    </tr>
                    <tr>
                    	<th colspan="4" class="bg-default" align="left" width="100%"><h5>Transactions</h5></th>
                    </tr>
                </thead>
            </table>
            <table class="table table-condensed details">
            	<thead>
                	<tr>
                    	<th width="33%"><h4>Order Id</h4></th>
                        <th width="33%"><h4>Date</h4></th>
                        <th width="33%"><h4>Points</h4></th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                    	<td><h4><a href="#" class="btn btn-primary">ORD00ABF00001</a></h4></td>
                        <td><h4>10<sup>th</sup> Mar 2016</h4></td>
                        <td><h4>2000</h4></td>
                    </tr>
                    <tr>
                    	<td><h4><a href="#" class="btn btn-primary">ORD00ABF00001</a></h4></td>
                        <td><h4>10<sup>th</sup> Mar 2016</h4></td>
                        <td><h4>2000</h4></td>
                    </tr>
                    <tr>
                    	<td><h4><a href="#" class="btn btn-primary">ORD00ABF00001</a></h4></td>
                        <td><h4>10<sup>th</sup> Mar 2016</h4></td>
                        <td><h4>2000</h4></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>