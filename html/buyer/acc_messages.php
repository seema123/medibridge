<div class="box">
	<div class="row header">
    	<div class="col-md-6 col-sm-6">
        	<h4>Your Messages</h4>
      	</div>
 	</div>
    <div class="tab-content">
        <div id="msg" class="tab-pane message fade in active">
            <div class="col-md-4 col-sm-4 msg_head">
               	<ul class="nav nav-pills nav-stacked">
                	<li class="active">
                    	<a href="#m1" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                    	</a>
                        <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    </li>
                    <li>
                    	<a href="#m2" data-toggle="tab">
                        	<h4>Medibridge <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
                    <li>
                    	<a href="#m3" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
                    <li>
                    	<a href="#m4" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
                    <li>
                    	<a href="#m5" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
                    <li>
                    	<a href="#m6" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
                    <li>
                    	<a href="#m7" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
                    <li>
                    	<a href="#m8" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
                    <li>
                    	<a href="#m9" data-toggle="tab">
                        	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                            <p>We will inform you once the items in your order have been shipped </p>
                            <button class="btn btn-link pull-right"><img src="images/delete.png"></button>
                    	</a>
                    </li>
              	</ul>
            </div>
            <div class="col-md-8 col-sm-8 msg_body">
                <div class="tab-content">
                  	<div class="tab-pane active" id="m1">
                    	<h4>Notification <span class="pull-right">4/04/2016</span></h4>
                        <p>Hi, you need to update your stock, Most of your products are going out of stock. You can update your stock from “View Your Store page”. Keep your stocks updated for better experience.</p>
                        <p>Reach Us : support@medibridge.com</p>
                    </div>
                  	<div class="tab-pane" id="m2">
                    	<h4>Medibridge <span class="pull-right">4/04/2016</span></h4>
                        <p>Hi, you need to update your stock, Most of your products are going out of stock. You can update your stock from “View Your Store page”. Keep your stocks updated for better experience.</p>
                        <p>Reach Us : support@medibridge.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>