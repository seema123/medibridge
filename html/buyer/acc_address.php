<div class="box">
	<!--<div class="row header">
    	<div class="col-md-6 col-sm-6">
        	<h4>Change Password</h4>
      	</div>
        <div class="col-md-6 col-sm-6 text-right">
        	<a href="#" class="btn1 whitebtn">Edit</a>
      	</div>
 	</div>-->
    <div class="tab-content">
        <div id="pwd" class="tab-pane info fade in active">
            <form name="frm-address_book" enctype="multipart/form-data" method="post" class="form-horizontal mb20">
            	<div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Full Name</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_fname" class="form-control" value="" placeholder="Full Name">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Address Heading</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_add_heading" class="form-control" value="" placeholder="eg. Andheri Branch Address">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Address Type</label>
                    </div>
                    <div class="col-md-4 col-sm-7 p0">
                    	<select class="form-control" name="cbo_addresstype">
                        	<option value="Billing Address">Billing Address</option>
                            <option value="Shipping Address">Shipping Address</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Address</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_address" class="form-control" value="" placeholder="Address">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Landmark</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_landmark" class="form-control" value="" placeholder="Landmark">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>City</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_city" class="form-control" value="" placeholder="City">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Pincode</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_pincode" class="form-control" value="" placeholder="Pincode">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>State</label>
                    </div>
                    <div class="col-md-4 col-sm-7 p0">
                    	<select class="form-control" name="cbo_state">
                        	<option value="Select">Select</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Country</label>
                    </div>
                    <div class="col-md-4 col-sm-7 p0">
                    	<select class="form-control" name="cbo_country">
                        	<option value="Select">Select</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Phone</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_phone" class="form-control" value="" placeholder="Phone">
                    </div>
                </div>
                <div class="form-group mt40">
                	<div class="col-md-12 col-sm-12 text-center">
                    	<button type="submit" class="btn orangebtn">Save</button>
                    </div>                	
                </div>
            </form>
            <div class="row address_book">
            	<div class="col-md-12 col-sm-12">
                	<h3>Shipping Address</h3>
                    <hr>
                    <table class="table">
                    	<tr>
                        	<td width="60%">
                            	<h4>Amit Kumar</h4>
                                <p>
                                	9896988672<br />
                                    Sea Hill, Bldg no. 2, flat no. 702, <br />Pleasant Park, Carter road, Andheri West, <br />Above Mad Over Donut<br /> Mumbai, Maharashtra - 400050
                                </p>
                            </td>
                            <td width="40%" align="right">
                            	<a href="#" class="btn1 whitebtn">Edit</a>
                                <a href="#" class="btn1 whitebtn">Remove</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>