<div class="box">
	<div class="row header">
    	<div class="col-md-6 col-sm-6">
        	<h4>Support</h4>
      	</div>
 	</div>
    <div class="tab-content">
        <div id="spt" class="tab-pane support fade in active">
            <div class="col-md-12 col-sm-12 sup_head">
                <h5>Thursday, 04<sup>th</sup> June, 2016</h5>
            </div>
            <div class="col-md-12 col-sm-12 sup_body">
                <p>Do you need help in listning your products?</p>
                <p>or not sure how to use our products? We're here to help you.</p>
                <br />
                <p class="graytext">Email:<br /><a href="#" class="orangetext">support@medibridge.com</a></p>
                <br />
                <p class="graytext">Phone:<br /><a href="#" class="orangetext">1800-997-6295</a></p>
            </div>
            <div class="col-md-12 col-sm-12 sup_form">
            	<h5>Write to us:</h5>
            	<form method="post" enctype="multipart/form-data" class="form-horizontal">
                	<div class="form-group">
                    	<div class="col-md-6 col-sm-8">
                        	<select name="cbo_topic" class="form-control">
                            	<option value="Select Topic">Select Topic</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    	<div class="col-md-6 col-sm-8">
                        	<textarea class="form-control" placeholder="Enter your message"></textarea>
                        </div>
                        <div class="col-md-4 col-sm-3 text-left p36">
                        	<input type="submit" class="btn orangebtn" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>