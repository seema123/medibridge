<div class="box">
    <div class="tab-content">
        <div id="pwd" class="tab-pane list fade in active">
        	<div class="col-md-12 col-sm-12 list_head">
            	<div class="form-group">
                	<div class="col-md-4 col-sm-5">
                    	<select class="form-control">
                        	<option value="All">All</option>
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-5">
                    	<input type="submit" class="btn pinkbtn" value="Search">
                    </div>
                </div> 
            </div>
            <div class="col-md-12 col-sm-12 list_head_sec text-right">
            	<h5>New shopping lists this month : 10</h5>
            </div>
            <div class="col-md-12 col-sm-12 list_head_thrd">
            	<table width="100%" class="table table-condensed">
                	<tr>
                    	<td width="50%">
                        	<h4><span class="graytext">TOTAL SHIPPING LIST</span> : 230</h4>
                        </td>
                        <td width="50%" class="text-right">
                        	<button type="submit" class="btn bluedarkbtn">Download Excel</button>
                            <button type="submit" class="btn greenbtn"><i class="glyphicon glyphicon-plus"></i> Add Shopping List</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12 col-sm-12 list_head_forth">
            	<table width="100%" class="table table-bordered">
                	<thead>
                    	<tr>
                        	<th valign="middle"><h4>Order ID</h4></th>
                            <th><h4>Shopping ID</h4></th>
                            <th><h4>No. of Items</h4></th>
                            <th><h4>Date Placed</h4></th>
                            <th><h4>Date Delivered</h4></th>
                            <th><h4>Dept Type</h4></th>
                            <th><h4>Status</h4></th>
                            <th><h4>Shopping Total</h4></th>
                            <th><h4>Action</h4></th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td><h5 class="btn btn-primary"><a href="#">ORD00ABF00001</a></h5></td>
                            <td><p>SPL002</p></td>
                            <td><p>780</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>10/04/2016</p></td>
                            <td><p>Dental</p></td>
                            <td><p>Delivered</p></td>
                            <td><h5><span class="rupee">₹</span> <strong>2000</strong></h5></td>
                            <td><a href="#"><img src="images/edit.png"></a></td>
                        </tr>
                        <tr>
                        	<td colspan="4" style="text-align:left;">
                            	<span><strong>1</strong> to <strong>20</strong> of <strong>1200</strong></span>
                            </td>
                            <td colspan="5" style="text-align:right;">
                            	<ul>
                                	<li><a href="#"><img src="images/left-a.png"></a></li>
                                    <li><a href="#">Previous</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">Next</a></li>
                                    <li><a href="#"><img src="images/right-a.png"></a></li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>