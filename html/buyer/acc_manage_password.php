<div class="box">
	<div class="row header">
    	<div class="col-md-6 col-sm-6">
        	<h4>Change Password</h4>
      	</div>
 	</div>
    <div class="tab-content">
        <div id="pwd" class="tab-pane info fade in active">
            <form name="frm-base-info" enctype="multipart/form-data" method="post" class="form-horizontal">
            	<div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Current Password</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_name" class="form-control" value="" placeholder="Current Password">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>New Password</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_email" class="form-control" value="" placeholder="New Password">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-5">
                    	<label>Confirm Password</label>
                    </div>
                    <div class="col-md-9 col-sm-7 brd p0">
                    	<input type="text" name="txt_phone" class="form-control" value="" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="form-group mt40">
                	<div class="col-md-12 col-sm-12 text-center">
                    	<button type="submit" class="btn orangebtn">Change</button>
                    </div>                	
                </div>
            </form>
        </div>
    </div>
</div>