<div class="box">
	<div class="row hr-strip">
    	<div class="col-md-8 col-sm-8">
        	<h4>need supplies specific to your feild?</h4>
            <h4>Try browsing our customized shopping list.</h4>
        </div>
        <div class="col-md-4 col-sm-4 text-right">
        	<a href="#" class="btn darkbluebtn">Upgrade now</a>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#info">Basic Info</a></li>
        <li><a data-toggle="tab" href="#company">Company Details</a></li>
        <li><a data-toggle="tab" href="#rm">RM Details</a></li>
    </ul>
    <div class="tab-content">
        <div id="info" class="tab-pane info fade in active">
            <form name="frm-base-info" enctype="multipart/form-data" method="post" class="form-horizontal">
            	<div class="form-group brd">
                	<div class="col-md-3 col-sm-3">
                    	<label>Name</label>
                    </div>
                    <div class="col-md-9 col-sm-9">
                    	<input type="text" name="txt_name" class="form-control" value="nishi">
                    </div>
                </div>
                <div class="form-group brd">
                	<div class="col-md-3 col-sm-3">
                    	<label>Email</label>
                    </div>
                    <div class="col-md-9 col-sm-9">
                    	<input type="text" name="txt_email" class="form-control" value="nishi.nishikant@gmail.com">
                    </div>
                </div>
                <div class="form-group brd">
                	<div class="col-md-3 col-sm-3">
                    	<label>Phone</label>
                    </div>
                    <div class="col-md-9 col-sm-9">
                    	<input type="text" name="txt_phone" class="form-control" value="+91 - 98765 43***">
                    </div>
                </div>
                 <div class="form-group brd">
                	<div class="col-md-3 col-sm-3">
                    	<label>Status</label>
                    </div>
                    <div class="col-md-9 col-sm-9">
                    	<input type="text" name="txt_status" class="form-control" value="General">
                    </div>
                </div>
                <div class="form-group mt40">
                	<div class="col-md-12 col-sm-12 text-center">
                    	<button type="submit" class="btn darkgraybtn">Edit</button>
                        <button type="submit" class="btn darkgraybtn">Update</button>
                    </div>                	
                </div>
            </form>
        </div>
        <div id="company" class="tab-pane company fade">
            <form name="frm-base-info" enctype="multipart/form-data" method="post" class="form-horizontal">
            	<div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Choose Department</label>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <select class="form-control" name="cbo_dept">
                            <option value="select">Select</option>
                        </select>
                    </div>
              	</div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Job Role</label>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <select class="form-control" name="cbo_dept">
                            <option value="Hospital Practitioner">Hospital Practitioner</option>
                            <option value="Individual Practitioner">Individual Practitioner</option>
                        </select>
                    </div>
              	</div>
                <div class="form-group">
                	<div class="col-md-12 col-sm-12">
                    	<label><strong>You have to submit below documents to have access to shopping list.</strong></label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Government Registration</label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <select class="form-control" name="cbo_dept">
                            <option value="select">Select</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="Select file" readonly>
                        <p class="greentext"><i class="acc_sprite chk_icn"></i> Adharcard.jpg <a href="#"><i class="acc_sprite remove_icn"></i></a></p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<label class="btn darkgraybtn btn-file">
                        	<span style="color:#fff;">Upload</span> <input type="file">
                        </label>
                        <p>(JPG, GIF, PNG & PDF format only)</p>
                    </div>
              	</div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>VAT/TIN Number</label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="" placeholder="MDB0001111">
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="Select file" readonly>
                        <p class="greentext"><i class="acc_sprite chk_icn"></i> Adharcard.jpg <a href="#"><i class="acc_sprite remove_icn"></i></a></p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<label class="btn darkgraybtn btn-file">
                        	<span style="color:#fff;">Upload</span> <input type="file">
                        </label>
                        <p>(JPG, GIF, PNG & PDF format only)</p>
                    </div>
              	</div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>CSI Certificate</label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="Select file" readonly>
                        <p class="greentext"><i class="acc_sprite chk_icn"></i> Adharcard.jpg <a href="#"><i class="acc_sprite remove_icn"></i></a></p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<label class="btn darkgraybtn btn-file">
                        	<span style="color:#fff;">Upload</span> <input type="file">
                        </label>
                        <p>(JPG, GIF, PNG & PDF format only)</p>
                    </div>
              	</div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Address Proof</label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <select class="form-control" name="cbo_dept">
                            <option value="select">Select</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="Select file" readonly>
                        <p class="greentext"><i class="acc_sprite chk_icn"></i> Adharcard.jpg <a href="#"><i class="acc_sprite remove_icn"></i></a></p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<label class="btn darkgraybtn btn-file">
                        	<span style="color:#fff;">Upload</span> <input type="file">
                        </label>
                        <p>(JPG, GIF, PNG & PDF format only)</p>
                    </div>
              	</div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Id Proof</label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <select class="form-control" name="cbo_dept">
                            <option value="select">Select</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="Select file" readonly>
                        <p class="greentext"><i class="acc_sprite chk_icn"></i> Adharcard.jpg <a href="#"><i class="acc_sprite remove_icn"></i></a></p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<label class="btn darkgraybtn btn-file">
                        	<span style="color:#fff;">Upload</span> <input type="file">
                        </label>
                        <p>(JPG, GIF, PNG & PDF format only)</p>
                    </div>
              	</div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Certificate</label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <select class="form-control" name="cbo_dept">
                            <option value="select">Select</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="Select file" readonly>
                        <p class="greentext"><i class="acc_sprite chk_icn"></i> Adharcard.jpg <a href="#"><i class="acc_sprite remove_icn"></i></a></p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<label class="btn darkgraybtn btn-file">
                        	<span style="color:#fff;">Upload</span> <input type="file">
                        </label>
                        <p>(JPG, GIF, PNG & PDF format only)</p>
                    </div>
              	</div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Drug License</label>
                        <p>(Only required if billing is done to a captive pharmacy)</p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<input type="text" name="txt_gov" class="form-control" value="Select file" readonly>
                        <p class="greentext"><i class="acc_sprite chk_icn"></i> Adharcard.jpg <a href="#"><i class="acc_sprite remove_icn"></i></a></p>
                    </div>
                    <div class="col-md-3 col-sm-3">
                    	<label class="btn darkgraybtn btn-file">
                        	<span style="color:#fff;">Upload</span> <input type="file">
                        </label>
                        <p>(JPG, GIF, PNG & PDF format only)</p>
                    </div>
              	</div>
                <div class="form-group mt40">
                	<div class="col-md-12 col-sm-12 text-center">
                    	<button type="submit" class="btn darkgraybtn">Edit</button>
                        <button type="submit" class="btn darkgraybtn">Update</button>
                    </div>                	
                </div>
            </form>
        </div>
        <div id="rm" class="tab-pane rm fade">
            <form name="frm-base-info" enctype="multipart/form-data" method="post" class="form-horizontal">
            	<div class="form-group">
                	<div class="col-md-3 col-sm-3">
                    	<label>Name</label>
                    </div>
                    <div class="col-md-4 col-sm-6">
                    	<input type="text" name="txt_name" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-3">
                    	<label>Mobile No.</label>
                    </div>
                    <div class="col-md-4 col-sm-6">
                    	<input type="text" name="txt_phone" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-3 col-sm-3">
                    	<label>Email</label>
                    </div>
                    <div class="col-md-4 col-sm-6">
                    	<input type="text" name="txt_email" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group mt40">
                	<div class="col-md-12 col-sm-12 text-center">
                    	<button type="submit" class="btn darkgraybtn">Edit</button>
                        <button type="submit" class="btn darkgraybtn">Update</button>
                    </div>                	
                </div>
            </form>
        </div>
    </div>
</div>