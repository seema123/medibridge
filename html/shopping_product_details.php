<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
  <li><a href="#">Lab Supplies </a></li>
  <li><a href="#">Beakers </a></li>
  <li class="active">Heavy Low Scale Beakers by Kimble Chase</li>
</ol>
<div class="container productDetailssec shoppingdetailswrap ">
  <div class="p20"> 
    <h1 class="prodtdtlheading">Heavy Low Scale Beakers by Kimble Chase <span>Available</span></h1>
    <!--product details-->
    <div class="row">
      <div class="col-sm-4 col-md-3 productimgwrap"> 
        <!-- Place somewhere in the <body> of your page --> 
        <!-- Place somewhere in the <body> of your page -->
        
       <!-- <div class="imglarge"> <img id="zoom_03" src="images/product/BD Vacutainer SST Tubes(367977).jpg" data-zoom-image="images/product/BD Vacutainer SST Tubes(367977).jpg"/> </div>
        <div id="gallery_01"> <a href="#" data-image="images/product/BD Vacutainer SST Tubes(367977).jpg"   class="active"> <img id="img_01" src="images/product/BD Vacutainer SST Tubes(367977).jpg"/> </a> <a href="#" data-image="images/product/BVF - WITH HME PAEDIATRIC(TV Range ml250-1000).jpg"  > <img id="img_01" src="images/product/BVF - WITH HME PAEDIATRIC(TV Range ml250-1000).jpg"/> </a> <a href="#" data-image="images/product/DISPOVAN SINGLE USE NEEDLES green.jpg"  > <img id="img_01" src="images/product/DISPOVAN SINGLE USE NEEDLES green.jpg"/> </a> <a href="#" data-image="images/product/Nutec Nebulizer Handyneb.jpg"  > <img id="img_01" src="images/product/Nutec Nebulizer Handyneb.jpg"/> </a> <a href="#" data-image="images/product/PORTABLE STEAM STERICLAVE (NON-E) ST1214MN.jpg"  > <img id="img_01" src="images/product/PORTABLE STEAM STERICLAVE (NON-E) ST1214MN.jpg"/> </a> <a href="#" data-image="images/product/proximate px skin stapler.jpg"  > <img id="img_01" src="images/product/proximate px skin stapler.jpg"/> </a> </div>-->
        <div id="sync1" class="owl-carousel">
          <div class="item"><img src="images/product.jpg" /></div>
          <div class="item"><img src="images/product2.jpg" /></div>
          <div class="item"><img src="images/product.jpg"  /></div>
        </div>
        <div id="sync2" class="owl-carousel">
          <div class="item"><img src="images/product.jpg" /></div>
          <div class="item"><img src="images/product2.jpg" /></div>
          <div class="item"><img src="images/product.jpg" /></div>
        </div>
      </div>
      <div class="col-sm-8 col-md-9 productdtlwrap">
        
        
        <div class="productdtlspecify"> 
          
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#productdtl" aria-controls="productdtl" role="tab" data-toggle="tab">Product Details</a></li>
            <li role="presentation"><a href="#specificationdtl" aria-controls="specificationdtl" role="tab" data-toggle="tab">Specifications</a></li>
          </ul>
          
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="productdtl">
              <div class="row">
                <div class="col-xs-9">
                  <h2 class="blacktext specitproductname">Heavy Low Scale Beakers by Kimble Chase</h2>
                  <h3 class="specy">BEAKER, GRIFFIN, LOW, BLU SCALE, 1000ML</h3>
                </div>
                <div class="col-xs-3 ">
                  <div class="downloadpdf"><a href=""><span class="sprite"></span><span>Download <br>
                    Full   Details</span></a></div>
                </div>
              </div>
              <ul class="list-orangeArrow">
                <li>Choose KIMAX® Low Form Griffin "Colorware" glass beakers for ease of identification in the lab</li>
                <li>These beakers offer excellent mechanical strength and durability, while providing high resistance to chemical attack and thermal shock</li>
                <li>Choose KIMAX® Low Form Griffin "Colorware" glass beakers for ease of identification in the lab</li>
                <li>These beakers offer excellent mechanical strength and durability, while providing high resistance to chemical attack and thermal shock</li>
                <li>Choose KIMAX® Low Form Griffin "Colorware" glass beakers for ease of identification in the lab</li>
                <li>These beakers offer excellent mechanical strength and durability, while providing high resistance to chemical attack and thermal shock</li>
                <li>Choose KIMAX® Low Form Griffin "Colorware" glass beakers for ease of identification in the lab</li>
                <li>These beakers offer excellent mechanical strength and durability, while providing high resistance to chemical attack and thermal shock</li>
              </ul>
              <div class="packagingbox">
                <div class="row">
                  <div class="col-xs-6">Packaging</div>
                  <div class="col-xs-6"><b>6 Each / Case</b></div>
                  <div class="col-xs-6">Manuf / Supplier</div>
                  <div class="col-xs-6"><b>Kimble Chase</b></div>
                  <div class="col-xs-6">Manuf / Supplier #</div>
                  <div class="col-xs-6"><b>14000B-1000</b></div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="specificationdtl">
              <h2 class="blacktext specitproductname ">Heavy Low Scale Beakers by Kimble Chase</h2>
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th scope="row">Latex Free</th>
                    <td>No</td>
                  </tr>
                  <tr>
                    <th scope="row">CAP</th>
                    <td>125Ml</td>
                  </tr>
                  <tr>
                    <th scope="row">COLOR</th>
                    <td>Green</td>
                  </tr>
                  <tr>
                    <th scope="row">CAP</th>
                    <td>125Ml</td>
                  </tr>
                  <tr>
                    <th scope="row">COLOR</th>
                    <td>Green</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
        
      </div>
    </div>
     <div class="row">
      <div class="col-sm-3  productdtlwrap"> 
          <ul class="dtlmanufacture">
          	<li><b>Manufacturer :</b> Kimble Chase</li>
            <li><b>Package Details : </b> 6 Each / Case</li>
          <!--  <li><b>Supplier Code : </b>UNI102561</li>-->
          </ul>
          
          <div class="brand100"><img src="images/svg/100.svg"> 100% Authentic Brands</div>
          
           <div class="tags">
          	<ul>
            	<li><strong>Tags</strong> :</li>
            	<li><a href="" target="_blank">Beakers</a></li>
                <li><a href="" target="_blank">Beakers</a></li>
                <li><a href="" target="_blank">Beakers</a></li>
                <!--<li><a href="" target="_blank">Beakers</a></li>
                <li><a href="" target="_blank">Beakers</a></li>-->
            </ul>
          </div>
        
      </div>
    <div class="col-sm-9 productdtlwrap">
        
        
        <div class="row">
          <div class="col-md-7 col-sm-12 ">
           
            <div class="pricehld">
              <input name="" type="button" class="btn borderbtn dtlBuyNowBtn" value="Add to List">
            </div>
            
            <div class="proddtlwrap">
            	<h5>Payment:</h5>
				<ul class="paymentcard">
                <li><img src="images/cardicn/citrus.jpg"  ></li>
			    <li><img src="images/cardicn/billdesk.jpg" ></li>
			    <li><img src="images/cardicn/visa.jpg" ></li>
			    <li><img src="images/cardicn/master-card.jpg"  ></li>
			    <li><img src="images/cardicn/cash.jpg" ></li>
                <li><img src="images/cardicn/netbanking.jpg" ></li>
				</ul>
            </div>
              
   
            
            <!--<div class="returnpolicyhld proddtlwrap">
            	<h5>Return Policy:</h5>
				<p>Returns accepted if product not as described, buyer pays return shipping fee; or keep the product & agree refund with seller.</p>
            </div>-->
            
            
            
            <!--<div class="buyerprotection">
        <h4>Buyer Protection</h4>
        <ul class="list-orangeRoundArrow">
          <li>Full Refund if you don't receive your order</li>
          <li>Refund or Keep items not as described</li>
        </ul>
      </div>-->
          </div>
         <div class="col-md-5 col-sm-12 proddtlright">
               
          
          <div class="pincodewrap proddtlwrap mt0">
          <h5 class="mt0">Enter pincode for delivery estimate to your location</h5>
          <div class="input-group">
          <span class="sprite"></span>
  <input type="text" class="form-control" placeholder="Enter Pincode" aria-describedby="basic-addon2">
  <span class="input-group-btn">
        <button class="btn  lightgraybtn" type="button">Check</button>
      </span>
  
</div>
          	<p class="graytext">Standard Delivery in 2-4 Business Days. Actual Delivery time is subject to your pincode location.</p>
             </div>
               <div class="returnpolicyhld mb20"> <a href="">Return Policy </a>  <b><i>7 days guaranteed return</i> </b></div>
             
         
          <!-- <div class="proddtlSellerInfo">
          <h2>SELLER INFORMATION</h2>
          <span class="soldbysec">Sold by : <a href="" class="bluetext">UNI102561</a></span>
          <ul class="star-ratings large mt10">
            <li class="active"></li>
            <li class="active"></li>
            <li class=""></li>
            <li class=""></li>
            <li class=""></li>
          </ul>
          <div class="row">
            <div class="col-xs-12">
              <div class="varifyicn"> <span  class="sprite varifiedimg "></span> <span class="">Verified</span></div>
              <span class="reviewcount">92%</span> <span  class="reviewstatus">Positive Reviews</span> </div>
          </div>
          <div class="dtlratings">
            <h4 class="blacktext">DETAILED MERCHANT RATING</h4>
            <h5 class="orangetext fontbold">Overall Rating:  3.87</h5>
            <div class="row overallratingpanel">
              <div class="col-xs-6">Shipping Time :</div>
              <div class="col-xs-5">
                <ul class="star-ratings">
                  <li class="active"></li>
                  <li class="active"></li>
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                </ul>
              </div>
              <div class="col-xs-1 p0">4.2</div>
            </div>
            <div class="row overallratingpanel">
              <div class="col-xs-6">Shipping Cost : </div>
              <div class="col-xs-5">
                <ul class="star-ratings">
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                </ul>
              </div>
              <div class="col-xs-1 p0">4.2</div>
            </div>
            <div class="row overallratingpanel">
              <div class="col-xs-6">Product Quality : </div>
              <div class="col-xs-5">
                <ul class="star-ratings">
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                </ul>
              </div>
              <div class="col-xs-1 p0">4.2</div>
            </div>
            <div class="row overallratingpanel">
              <div class="col-xs-6">Value for Money :</div>
              <div class="col-xs-5">
                <ul class="star-ratings">
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                  <li class=""></li>
                </ul>
              </div>
              <div class="col-xs-1 p0">4.2</div>
            </div>
          </div>
        </div>-->
          
      
          
          </div>
        </div>
      </div>
     </div>
  </div>
  <div class="p20"> 
    <!--other varient-->
    <div class="othervarientbl">
      <h4 class="blacktext">Other Variants</h4>
      <div class="table-responsive ">
      <table class="table">
        <tr>
          <th class="varientimg"></th>
          <th>ID</th>
          
          <th>Discription</th>
          <!--<th>Price</th>-->
          <th width="204">Quantity</th>
         <th width="205" class="text-center">Action</th>
        </tr>
        <tr>
          <td width="94" class="varientimg"><img src="images/product_300.jpg" class="img-responsive" ></td>
          <td width="134" class="varientcode"><a href="">MSC097062H</a></td>
          <td width="469" class="varientdic">
         
          <p><b>Type : </b> Borosilicate Glass</p>
          <p><b>Size : </b> 100 ML</p>
          <p><b>Color : </b> Black</p>
          </td>
           
          <!--<td class="varientprice"><div class="productwrapper text-left">
              <div class="pricehld">
                <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
              </div>
            </div> 
            <div class="bulkorderpop dropdown">
            <a id="dLabel" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Bulk order <span class="caret "></span></a>
            	 
<ul   class="dropdown-menu prodctPricehld" aria-labelledby="dLabel">
               
              <li>
                <div class="productwrapper">
                <div class="qutyperprice">Qty : 20-40</div>
                  <div class="pricehld">
                    <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                     
                  </div>
                </div>
              </li>
              <li>
                <div class="productwrapper">
                <div class="qutyperprice"> Qty :  40-80</div>
                  <div class="pricehld">
                    <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                     
                  </div>
                </div>
              </li>
              <li>
                <div class="productwrapper">
                <div class="qutyperprice">Qty : 80 & Above</div>
                  <div class="pricehld">
                    <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                    
                  </div>
                </div>
              </li>
            </ul>
   
            </div>
            </td>-->
          <td class="varientquantity"><div class="quantitywrap">
              <input type='button' value='-' class='qtyminus btn' field='quantity' />
              <input type='text' name='quantity' value='1' class='qty' />
              <input type='button' value='+' class='qtyplus btn' field='quantity' />
            </div></td>
          <td class="varientbtn text-center"><button class="btn borderbtn">Add to List</button>
            <a href="#" class="moredtl"> More details</a></td>
        </tr>
        
         
         
      </table>
      </div>
    </div>
  </div>
   
</div>

<!--sell on medibridge-->

<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>
