<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
  <li><a href="#">Lab Supplies </a></li>
  <li><a href="#">Beakers </a></li>
  <li class="active">Heavy Low Scale Beakers by Kimble Chase</li>
</ol>
<div class="container">
  <div class="bgwhite mylistwrap">
    <div class="p20"> 
      <!--other varient-->
      <div class="othervarientbl">
        <h4 class="blacktext mb0">My Nursing Home List <span class="graytext">(52)</span> </h4>
        <h6 class="graytext">Last upadate on 15 DEC. 2015</h6>
        <ul class="mylistoption">
          <li><img src="images/svg/cart1.svg" width="22" height="22"> Add all items to cart</li>
          <li data-toggle="modal" data-target="#popup"><img src="images/svg/edit.svg" width="16" height="16"> Rename list</li>
 
          <li><img src="images/svg/remove.svg" width="16" height="16"> Remove all items</li>
          <li><img src="images/svg/bucket.svg" width="16" height="16"> Delet list</li>
          <li><img src="images/svg/add.svg" width="16" height="16"> Add items</li>
        </ul>
   <!--Rename popup start-->       
 <div class="modal fade smallpopup" id="popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
<div class="modal-body">
 <form>
  <div class="form-group">
    <label for="exampleInputEmail1">Rename My list</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <button type="submit" class="btn orangebtn">Submit</button>
</form>
       
      </div>
    </div>
  </div>
</div>         
      <!--Rename popup end-->   
        
        <div class="table-responsive ">
          <table class="table">
            <thead>
              <tr>
                <th class="varientimg">Product</th>
                <th></th>
                <th>Name</th>
                <th>Brand</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total Price</th>
                <th class="text-center">Delete</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="varientimg"><img src="images/product_300.jpg" class="img-responsive" ></td>
                <td class="varientcode"><a href="">MSC097062H</a></td>
                <td class="varientdic font14"> Heavy Low Scale Beakers by
                  <div> Kimble Chase </div></td>
                <td class="">SISCO </td>
                <td class="varientprice"><div class="productwrapper text-left">
                    <div class="pricehld">
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                  </div>
                  <div class="bulkorderpop dropdown"> <a id="dLabel" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Bulk order <span class="caret "></span></a>
                    <ul   class="dropdown-menu prodctPricehld" aria-labelledby="dLabel">
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 20-40</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice"> Qty :  40-80</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 80 & Above</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div></td>
                <td class="varientquantity"><div class="quantitywrap">
                    <input type='button' value='-' class='qtyminus btn' field='quantity' />
                    <input type='text' name='quantity' value='1' class='qty' />
                    <input type='button' value='+' class='qtyplus btn' field='quantity' />
                  </div></td>
                <td><b class="font16"><span class="rupee">₹</span> 500.00/-</b></td>
                <td class="text-center"><a href="#"><img src="images/svg/bucket.svg" width="20" height="20"></a></td>
              </tr>
              <tr>
                <td class="varientimg"><img src="images/product_300.jpg" class="img-responsive" ></td>
                <td class="varientcode"><a href="">MSC097062H</a></td>
                <td class="varientdic font14"> Heavy Low Scale Beakers by
                  <div> Kimble Chase </div></td>
                <td class="">SISCO </td>
                <td class="varientprice"><div class="productwrapper text-left">
                    <div class="pricehld">
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                  </div>
                  <div class="bulkorderpop dropdown"> <a id="dLabel" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Bulk order <span class="caret "></span></a>
                    <ul   class="dropdown-menu prodctPricehld" aria-labelledby="dLabel">
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 20-40</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice"> Qty :  40-80</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 80 & Above</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div></td>
                <td class="varientquantity"><div class="quantitywrap">
                    <input type='button' value='-' class='qtyminus btn' field='quantity' />
                    <input type='text' name='quantity' value='1' class='qty' />
                    <input type='button' value='+' class='qtyplus btn' field='quantity' />
                  </div></td>
                <td><b class="font16"><span class="rupee">₹</span> 500.00/-</b></td>
                <td class="text-center"><a href="#"><img src="images/svg/bucket.svg" width="20" height="20"></a></td>
              </tr>
              <tr>
                <td class="varientimg"><img src="images/product_300.jpg" class="img-responsive" ></td>
                <td class="varientcode"><a href="">MSC097062H</a></td>
                <td class="varientdic font14"> Heavy Low Scale Beakers by
                  <div> Kimble Chase </div></td>
                <td class="">SISCO </td>
                <td class="varientprice"><div class="productwrapper text-left">
                    <div class="pricehld">
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                  </div>
                  <div class="bulkorderpop dropdown"> <a id="dLabel" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Bulk order <span class="caret "></span></a>
                    <ul   class="dropdown-menu prodctPricehld" aria-labelledby="dLabel">
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 20-40</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice"> Qty :  40-80</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 80 & Above</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div></td>
                <td class="varientquantity"><div class="quantitywrap">
                    <input type='button' value='-' class='qtyminus btn' field='quantity' />
                    <input type='text' name='quantity' value='1' class='qty' />
                    <input type='button' value='+' class='qtyplus btn' field='quantity' />
                  </div></td>
                <td><b class="font16"><span class="rupee">₹</span> 500.00/-</b></td>
                <td class="text-center"><a href="#"><img src="images/svg/bucket.svg" width="20" height="20"></a></td>
              </tr>
              <tr>
                <td class="varientimg"><img src="images/product_300.jpg" class="img-responsive" ></td>
                <td class="varientcode"><a href="">MSC097062H</a></td>
                <td class="varientdic font14"> Heavy Low Scale Beakers by
                  <div> Kimble Chase </div></td>
                <td class="">SISCO </td>
                <td class="varientprice"><div class="productwrapper text-left">
                    <div class="pricehld">
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                  </div>
                  <div class="bulkorderpop dropdown"> <a id="dLabel" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Bulk order <span class="caret "></span></a>
                    <ul   class="dropdown-menu prodctPricehld" aria-labelledby="dLabel">
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 20-40</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice"> Qty :  40-80</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 80 & Above</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div></td>
                <td class="varientquantity"><div class="quantitywrap">
                    <input type='button' value='-' class='qtyminus btn' field='quantity' />
                    <input type='text' name='quantity' value='1' class='qty' />
                    <input type='button' value='+' class='qtyplus btn' field='quantity' />
                  </div></td>
                <td><b class="font16"><span class="rupee">₹</span> 500.00/-</b></td>
                <td class="text-center"><a href="#"><img src="images/svg/bucket.svg" width="20" height="20"></a></td>
              </tr>
              <tr>
                <td class="varientimg"><img src="images/product_300.jpg" class="img-responsive" ></td>
                <td class="varientcode"><a href="">MSC097062H</a></td>
                <td class="varientdic font14"> Heavy Low Scale Beakers by
                  <div> Kimble Chase </div></td>
                <td class="">SISCO </td>
                <td class="varientprice"><div class="productwrapper text-left">
                    <div class="pricehld">
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                  </div>
                  <div class="bulkorderpop dropdown"> <a id="dLabel" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Bulk order <span class="caret "></span></a>
                    <ul   class="dropdown-menu prodctPricehld" aria-labelledby="dLabel">
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 20-40</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice"> Qty :  40-80</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 80 & Above</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div></td>
                <td class="varientquantity"><div class="quantitywrap">
                    <input type='button' value='-' class='qtyminus btn' field='quantity' />
                    <input type='text' name='quantity' value='1' class='qty' />
                    <input type='button' value='+' class='qtyplus btn' field='quantity' />
                  </div></td>
                <td><b class="font16"><span class="rupee">₹</span> 500.00/-</b></td>
                <td class="text-center"><a href="#"><img src="images/svg/bucket.svg" width="20" height="20"></a></td>
              </tr>
              <tr>
                <td class="varientimg"><img src="images/product_300.jpg" class="img-responsive" ></td>
                <td class="varientcode"><a href="">MSC097062H</a></td>
                <td class="varientdic font14"> Heavy Low Scale Beakers by
                  <div> Kimble Chase </div></td>
                <td class="">SISCO </td>
                <td class="varientprice"><div class="productwrapper text-left">
                    <div class="pricehld">
                      <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                      <div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span> <span class="discountPrice">40% Off</span></div>
                    </div>
                  </div>
                  <div class="bulkorderpop dropdown"> <a id="dLabel" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Bulk order <span class="caret "></span></a>
                    <ul   class="dropdown-menu prodctPricehld" aria-labelledby="dLabel">
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 20-40</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice"> Qty :  40-80</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="productwrapper">
                          <div class="qutyperprice">Qty : 80 & Above</div>
                          <div class="pricehld">
                            <div class="perPiece"><span class="rupee">₹</span>6,500/<span class="perp">per piece</span></div>
                            <!--<div class=""><span class="oldPrice"><span class="rupee">₹</span>8,500 </span><span class="discountPrice">40% Off</span> </div>--> 
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div></td>
                <td class="varientquantity"><div class="quantitywrap">
                    <input type='button' value='-' class='qtyminus btn' field='quantity' />
                    <input type='text' name='quantity' value='1' class='qty' />
                    <input type='button' value='+' class='qtyplus btn' field='quantity' />
                  </div></td>
                <td><b class="font16"><span class="rupee">₹</span> 500.00/-</b></td>
                <td class="text-center"><a href="#"><img src="images/svg/bucket.svg" width="20" height="20"></a></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="row mylisttotal">
          <div class="col-sm-6 col-sm-offset-6">
            <ul>
              <li>
                <p>Total quantity</p>
                <b> 265</b></li>
              <li>
                <p>Total Price</p>
                <b><span class="rupee">₹</span>8,500</b></li>
            </ul>
            <button value="" class="btn orangebtn">Place Order</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include 'footer.php';
?>
