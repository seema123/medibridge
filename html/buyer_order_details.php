<?php
	include 'header.php';
?>
<!--bredcrumbs-->
<ol class="breadcrumb container">
  	<li><a href="#">Home</a></li>
  	<li><a href="#">Orders </a></li>
  	<li class="active">ORD00ABF00001</li>
</ol>
<div class="container orderDetailssec">
    <!--Order details-->
    <div class="row top-cancel">
    	<div class="col-md-12 col-sm-12">
        	<table width="100%">
            	<tr>
                	<td width="50%" class="text-left">
                    	<h2>Partial Cancelled</h2>
                   	</td>
                    <td width="50%" class="text-right">
                    	17<sup>th</sup> March 2016
                   	</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row head-ord p20 mb20">
        <div class="col-md-12 col-sm-12">
            <h1>Order Details</h1>
            <ul>
                <li>Ordered on 17<sup>th</sup> March 2016</li>
                <li>|</li>
                <li><span class="redtext">Date of arrival For Pick Up 17<sup>th</sup> March 2016</span></li>
                <li>|</li>
                <li>Order #4000041234-1234</li>
                <li>|</li>
                <li>3 Items</li>
                <li>|</li>
                <li>Multiple Drops</li>
            </ul>
        </div>
        <div class="col-md-12 col-sm-12 orderAdd">
            <div class="col-sm-5">
                <h4>Billing Address</h4>
                <p>Nishi Kant</p>
                <p>98765 43210</p>
                <p>Shree Shaswat, Blnd no. 02, flat no. 10,</p>
                <p>Pleasant Park, Carter road, Bandra west.</p>
                <p>Mumbai, Maharahstra - 400 050.</p>
            </div>
            <div class="col-sm-4">
            	 <h4>Payment Mode</h4>
                 <p>Visa</p>
            </div>
            <div class="col-sm-3">
                <h4>Order Summary</h4>
                <p>Item(s) Subtotal : Rs. 8550.00</p>
                <p class="redtext">Cancelled Total : Rs. -4000.00</p>
                <p>Shipping charges : Rs. 950.00</p>
                <p>Total : Rs. 9550.00</p>
                <p class="mt10"><strong>Grand Total : Rs. 9550.00</strong></p>
            </div>
        </div>
    </div>
    <div class="row body-ord">
    	<div class="col-md-12 col-sm-12 box-heading">
        	<h3>Product Details</h3>
        </div>
 	</div>
    <div class="row body-ord">
        <div class="prd-details">
        	<table width="100%">
            	<tr>
                	<td width="10%">
                    	<a href="#">
                        	<img src="images/product4.jpg" width="100">
                        </a>
                    </td>
                    <td width="30%" class="rbrd">
                    	<a href="#" class="bluetext text-left">
                        	<strong>Solstice Nitril Powder <br />- Free Exam Gloves</strong>
                        </a>
                    </td>
                    <!--<td width="10%" class="text-center rbrd">
                    	<span><strong>SKU Number</strong></span><br />
                        <span><strong>79867543677</strong></span>
                    </td>-->
                    <td width="10%" class="text-center rbrd">
                    	<span><strong>Cipla</strong></span>
                    </td>
                    <td width="15%" class="text-center rbrd">
                    	<span><strong>Blue</strong></span>
                    </td>
                    <td width="15%" class="text-center rbrd">
                    	<span><strong>Latex</strong></span>
                    </td>
                    <td width="20%" class="text-center rbrd">
                    	<span><strong>Rs. 4000</strong></span>
                    </td>
                     <!-- <td width="20%" class="text-center rbrd"> <strong>Action</strong></td>-->
                </tr>
            </table>
        </div>
    </div>
    <div class="row content-ord p20">
    	<div class="col-md-2 data-ord text-center">
        	<h3>3 Box</h3>
        </div>
        <div class="col-md-4 data-ord">
        	<p>Nishi Kant, 98765 43210</p>
            <p>Shree Shaswat, Blnd no. 02, flat no. 10,</p>
            <p>Pleasant Park, Carter road, Bandra west.</p>
            <p>Mumbai, Maharahstra - 400 050.</p>
        </div>
        <div class="col-md-2 data-ord text-center">
        	<span><strong>Tracking Id:</strong><br />5476876874686</span>
            <p><a href="" class="bluetext ">Track this product</a></p>
        </div>
        <div class="col-md-2 data-ord text-center">
        	<span><strong>Delivered On</strong><br />17<sup>th</sup> March 2016</span>
        </div>
        <div class="col-md-2 data-ord text-center">
        	 
            <input type="submit" value="Cancel" class="btn lightgraybtn">
        </div>
 
    </div>
    <div class="row content-ord p20">
    	<div class="col-md-2 data-ord text-center">
        	<h3>3 Box</h3>
        </div>
        <div class="col-md-4 data-ord">
        	<p>Nishi Kant, 98765 43210</p>
            <p>Shree Shaswat, Blnd no. 02, flat no. 10,</p>
            <p>Pleasant Park, Carter road, Bandra west.</p>
            <p>Mumbai, Maharahstra - 400 050.</p>
        </div>
        <div class="col-md-2 data-ord text-center">
        	<span><strong>Tracking Id:</strong><br />5476876874686</span>
            <p><a href="" class="bluetext ">Track this product</a></p>
        </div>
        <div class="col-md-2 data-ord text-center">
        	<span><strong>Delivered On</strong><br />17<sup>th</sup> March 2016</span>
        </div>
        <div class="col-md-2 data-ord text-center">
        	 
            <input type="submit" value="Cancel" class="btn lightgraybtn">
        </div>
 
    </div>
    <div class="row content-ord p20">
    	<div class="col-md-2 data-ord text-center">
        	<h3>3 Box</h3>
        </div>
        <div class="col-md-4 data-ord">
        	<p>Nishi Kant, 98765 43210</p>
            <p>Shree Shaswat, Blnd no. 02, flat no. 10,</p>
            <p>Pleasant Park, Carter road, Bandra west.</p>
            <p>Mumbai, Maharahstra - 400 050.</p>
        </div>
        <div class="col-md-2 data-ord text-center">
        	<span><strong>Tracking Id:</strong><br />5476876874686</span>
            <p><a href="" class="bluetext ">Track this product</a></p>
        </div>
        <div class="col-md-2 data-ord text-center">
        	<span><strong>Delivered On</strong><br />17<sup>th</sup> March 2016</span>
        </div>
        <div class="col-md-2 data-ord text-center">
        	 
           <p><input type="submit" value="Return" class="btn lightgraybtn mb10"></p>
            <input type="submit" value="Review" class="btn lightgraybtn">
        </div>
 
    </div>
    <div class="text-right mt20">
  <input type="submit" value="Re-order" class="btn orangebtn ">
  </div>
</div>
<!--sell on medibridge-->
<?php
	include 'footer-top.php';
?>
<?php
	include 'footer.php';
?>
