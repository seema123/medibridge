<?php
	include 'header.php';
?>
<!--bredcrumbs-->
<ol class="breadcrumb container">
  	<li><a href="#">Home</a></li>
  	<li class="active">404 Error Page</li>
</ol>
<section class="Error404">
    <div class="container p20">
        <h1 class="orangetext">404</h1>
        <h2 class="orangetext">Page Not Found</h2>
    </div>
</section>
<?php
	include 'footer-top.php';
?>
<?php
	include 'footer.php';
?>