
		<?php include 'header.php'; ?>
		<div class="container" id="checkoutBox">
        	<div class="row">
            	<div class="col-md-12">
                	<div class="row nav-box">
                        <div class="col-md-12">
                        	<ul class="checkoutNav">
                            	<li class="active">
                                	<a href="checkout-shoppingcart.php"><h4><span class="iconbox">1</span> Shopping Cart</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li class="active">
                                	<a href="checkout-yourdetails.php"><h4><span class="iconbox">2</span> Your Details</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li class="active">
                                	<a href="checkout-paymethod.php"><h4><span class="iconbox">3</span> Payment Methods</h4></a>
                                </li>
                                <li><img src="images/nav-arrow.png"></li>
                                <li class="active">
                                	<a href="checkout-confirmation.php"><h4><span class="iconbox">4</span> Comfirmation</h4></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row nav-box-confirm">
                    	<div class="col-md-12 col-sm-12 text-center">
                        	<div class="confirm-box">
                            	<h2>Thank you for shopping with us.</h2>
                                <h3 class=" ">Your order is pending. <br>
Please send us net banking transaction id at <a href="mailto:info@clinito.com">info@clinito.com</a></h3>
                                
                                <h4 align="left">Your Order Details:</h4>
                                <div class=" confirm-order-details text-left">
                                    <p><strong>Order No.</strong>: XXX000</p>
                                    <p><strong>Paymode</strong>: NEFT</p>
                                    <p><strong>Amount Pending</strong>: <span class="rupee">₹</span> 15,345.00</p>
                                </div>
                                 
                            </div>
                            <div class="clearfix"></div>
                        </div>
                 	</div>
                    <div class="row nav-box-shopping text-center">
                    	<a href="#" class="btn orangebtn">Continue Shopping</a>
                    </div>
                </div>
           	</div>
		</div>
		<?php include 'footer.php'; ?>