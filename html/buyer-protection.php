<?php
include 'header.php';
?>

<!--bredcrumbs-->
<ol class="breadcrumb container">
  <li><a href="#">Home</a></li>
  <li class="active">Buyer protection</li>
</ol>
<div class="buyerprotectionwrap">
 <div class="container">
  <div class="innercontent">
  <h1 class="titleh">Buyer Protection</h1>
    <div class="bgwhite">
      <div class="p20">
        <div class="p20 text-center buyerprotections">
        
          <h4>Welcome to CLINITO’s buyer protection<br>
            With buyer protection you can guarantee a safe and secure purchase with the order reaching you on time and as directed.</h4>
          <h2 class="text-center orangetext mb20">BENEFITS</h2>
          <ul class="list-orangeRoundArrow text-left row">
            <li class="col-sm-5">
              <h5 class="mt0">Full refund in case the order isn’t delivered.</h5>
              <p>You are eligible for a full refund of your payment you 
                do not receive your order within the specified timeline.</p>
            </li>
            <li class="col-sm-6 col-sm-offset-1 ">
              <h5 class="mt0">Full or partial refund if the order delivered is not satisfactory.</h5>
              <p>You have the option of getting a full refund by returning the product you find unsatisfactory or get a partial refund in case you decide to keep the product regardless. </p>
            </li>
          </ul>
          <h2 class="text-center orangetext mb20">ADDITIONAL BENEFITS </h2>
          <ul class="list-orangeRoundArrow text-left row">
            <li class="col-sm-5">
              <h5 class="mt0">Domestic Returns</h5>
              <p>Products marked under this category can be returned domestically provided they haven’t been used and are in their original packaging. </p>
            </li>
            <li class="col-sm-6 col-sm-offset-1 ">
              <h5 class="mt0">Genuine guarantee </h5>
              <p>Products marked under this category if found to be substandard make you eligible for a full refund. </p>
            </li>
          </ul>
          
          
        </div>
      </div>
    </div>
    <div class="sellertabcontain">
      <div class="p20">
        <h2 class="text-center orangetext mb20">HOW IT WORKS</h2>
        
        <!-- Tab panes -->
        
        <div class="row">
          <div class="col-sm-6">
            <div class="media advantagebox">
              <div class="media-left"><img src="images/seller/call.jpg" ></div>
              <div class="media-body">
                <h3>STEP 1  :  Contact </h3>
                <p>Contact the seller in case of any discrepancy with the order pertaining to time of delivery, quality etc. </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="media advantagebox">
              <div class="media-left"><img src="images/seller/flag.jpg" ></div>
              <div class="media-body">
                <h3>STEP 2 : Open confrontation </h3>
                <p>Submit a formal complaint regarding the order if you are not able to resolve the situation with the seller. </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="media advantagebox">
              <div class="media-left"><img src="images/seller/assist.jpg" ></div>
              <div class="media-body">
                <h3>STEP 3 : Request CLINITO’s assistance </h3>
                <p>Request CLINITO’s intervention if the solution provided by seller is unsatisfactory. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
<?php
include 'footer-top.php';
?>
<?php
include 'footer.php';
?>
