
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Clinito | Home</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <script type="text/javascript">
			function isNumberKey(evt)
			   {
				  var charCode = (evt.which) ? evt.which : event.keyCode;
				  if (charCode != 46 && charCode > 31 
					&& (charCode < 48 || charCode > 57))
					{
						alert("Enter Only Numbers...!!!");             
						return false;
					}
				return true;
			   }
		</script>
	</head>
    
	<?php
		$popVisi = "hidden";
		$box2 = "visible";
    	if(isset($_POST['btn_submit'])) {
			include 'contact_mail.php';
			$popVisi = "visible";
			//$box2 = "hidden";
		}
     ?>
    
    <body>
    	<div class="container mainSec">
        	<div class="row wrapper">
            	<div class="col-md-6 col-sm-12 p20 text-center box1">
                	<a href="#" class="logo"><img src="images/Clinito_Final_Logo.svg" alt="Clinito"></a>
                    <h4 class="mb20">We are coming soon to transform your business,<br />Come witness the next level of healthcare<br />With CLINITO!</h4>
                    <h4 class="orangetext">Stay tuned...</h4>
                </div>
                <div class="col-md-6 col-sm-12 p20 text-center">
                	<div class="box2" id="boxform" style="visibility:<?php echo $box2; ?>">
                        <p>If you wish to be a part of <a href="#" class="orangetext">CLINITO</a>,<br />please leave us your details and we will<br />get back to you shortly.</p>
                        <form class="form-horizontal" enctype="multipart/form-data" name="frm-contact" method="post">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" name="txt_name" placeholder="Name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" name="txt_org" placeholder="Organization" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="tel" name="txt_contact" placeholder="Contact" class="form-control" required onkeypress="return isNumberKey(event)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="email" name="txt_email" placeholder="Email" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 text-left">
                                    <button class="btn orangebtn" name="btn_submit">Submit</button>
                                </div>
                            </div>
                        </form>
                        <p>Or write us at <a href="mailto:info@clinito.com" class="bluetext">info@clinito.com</a></p>
                   	</div>
                </div>
            </div>
            <div class="box3" id="popup" style="visibility:<?php echo $popVisi; ?>">
                <div class="box3-content">
                    <h4 class="bluetext mb20">Thank you for expressing interest in <a href="#" class="orangetext">CLINITO</a></h4>
                    <p>Your details have been successfully submitted<br />and we will get in touch with you shortly.</p>
                </div>
                <button type="button" class="close" onclick="div_hide()">&times;</button>
            </div>
        </div>
        
        <!-- jQuery 2.2.0 --> 
        <script src="js/jquery.min.js"></script>
        <!-- Bootstrap 3.3.6 --> 
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
			//function to hide Popup
			function div_hide(){ 
				document.getElementById('popup').style.visibility = "hidden";
				document.getElementById('boxform').style.visibility = "visible";
			}
		</script>
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		 
		  ga('create', 'UA-81285333-1', 'auto');
		  ga('send', 'pageview');
		</script>
    </body>
</html>
